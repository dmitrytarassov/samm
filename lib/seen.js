"use strict";

const assert = require("./assert.js");

const DEFAULT_SEEN_BUFFER_SIZE = 100;

class Seen {
    constructor(max) {
        assert.parameterTypeOf("Seen()", "max", max, "number");

        this.max = max;
        this._seen = [];
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    alreadySeen(id) {
        if (!id) {
            // Just ignore undefined
            return false;
        }

        return this._seen.includes(id.toString());
    }

    markSeen(id) {
        if (!id) {
            // Just ignore undefined
            return;
        }

        let seen = this._seen;
        seen.push(id.toString());
        if (seen.length > this.max) {
            seen = seen.slice(seen.length - this.max, seen.length);
            this._seen = seen;
        }
    }

    alreadySeenOrMark(id) {
        if (this.alreadySeen(id)) {
            return true;
        }

        this.markSeen(id);
        return false;
    }
}

module.exports = {
    DEFAULT_SEEN_BUFFER_SIZE,
    Seen
};