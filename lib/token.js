"use strict";

const assert = require("./assert.js");
const Big = require("big.js");
const enums = require("../lib/enums.js");
const itemcache = require("../lib/itemcache.js");
const numbers = require("../lib/numbers.js");
const serum = require("@project-serum/serum");
const solana = require("@solana/web3.js");
const tags = require("../lib/tags.js");

const SOLANA_SYSTEMPROGRAM_SPACE = 165;
const SOL_DECIMALS = 9;

const TOKEN_PROGRAM_ID = new solana.PublicKey(
    "TokenkegQfeZyiNwAJbNbGKPFXCWuBvf9Ss623VQ5DA"
);

class TokenAccount {
    constructor(publicKey, account) {
        assert.parameterType("TokenAccount()", "publicKey", publicKey, solana.PublicKey);
        // I'd really like to have the account strongly-typed, but it seems what we
        // get back from calls to findBaseTokenAccountsForOwner()/findQuoteTokenAccountsForOwner()
        // isn't really a solana.Account object. Instead we typically get something like:
        // {
        //     executable: false,
        //     owner: PublicKey {
        //       _bn: <BN: 6ddf6e1d765a193d9cbe146ceeb79ac1cb485ed5f5b37913a8cf5857eff00a9>
        //     },
        //     lamports: 2039280,
        //     data: <Buffer 12 8b cb 64 7d 8b 00 a2 19 61 ... 155 more bytes>
        //   }
        // As a compromise, we at least check we've got an 'owner' public key.
        assert.parameterType("TokenAccount()", "account.owner", account?.owner, solana.PublicKey);

        this.isTokenAccount = true;

        this.publicKey = publicKey;
        this.account = account;

        tags.ensureTags(this);

        Object.freeze(this);
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    equals(toCompare) {
        if (!toCompare?.publicKey) {
            return false;
        }

        return this.publicKey.equals(toCompare.publicKey) &&
            this.account.owner.equals(toCompare?.account?.owner);
    }

    toString() {
        return `« TokenAccount: ${this.publicKey.toBase58()} »`;
    }
}


const TokenSource = enums.createEnum(
    "TokenSource",
    [
        new enums.EnumValue("testing"),
        new enums.EnumValue("samm"),
        new enums.EnumValue("serumjs"),
        new enums.EnumValue("pool"),
        new enums.EnumValue("additionaltokenfile"),
        new enums.EnumValue("commandline")
    ]
);


class Token {
    constructor(symbol, address, source) {
        assert.parameterTypeIsString("Token()", "symbol", symbol);
        assert.parameterType("Token()", "address", address, solana.PublicKey);
        assert.parameterTypeIsEnum("Token()", "source", source, TokenSource);

        this.isToken = true;

        this.symbol = symbol;
        this.address = address;

        tags.ensureTags(this);
        tags.CommonTagOperations.source(this, source);

        if (new.target === Token) {
            Object.freeze(this);
        }
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    equals(anotherToken) {
        if (!anotherToken) {
            return false;
        }

        return this.symbol === anotherToken.symbol &&
            this.address.equals(anotherToken.address);
    }

    toString() {
        return `« ${this.symbol} at ${this.address} [${this.tags}]] »`;
    }
}

class TokenValue {
    static createFromQuantity(token, quantity, decimals) {
        assert.parameterType("TokenValue()", "token", token, Token);
        assert.parameterType("TokenValue()", "quantity", quantity, Big);
        assert.parameterTypeOf("TokenValue()", "decimals", decimals, "number");

        const decimalPower = new Big(numbers.DECIMAL_RADIX).pow(decimals);
        const actualQuantity = quantity.div(decimalPower);

        return new TokenValue(token, actualQuantity, decimals);
    }

    constructor(token, value, decimals) {
        assert.parameterType("TokenValue()", "token", token, Token);
        assert.parameterType("TokenValue()", "value", value, Big);
        assert.parameterTypeOf("TokenValue()", "decimals", decimals, "number");

        this.isTokenValue = true;

        this.token = token;
        this.value = value;
        this.decimals = decimals;

        tags.ensureTags(this);

        if (new.target === TokenValue) {
            Object.freeze(this);
        }
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    equals(anotherTokenValue) {
        if (!anotherTokenValue) {
            return false;
        }

        return this.token === anotherTokenValue.token &&
            this.value.eq(anotherTokenValue.value) &&
            this.decimals === anotherTokenValue.decimals;
    }

    roundQuantity(quantity) {
        assert.parameterType("Token.roundQuantity()", "quantity", quantity, Big);

        return quantity.round(this.decimals);
    }

    add(toAdd) {
        assert.parameterIsDefined("TokenValue.add()", "toAdd", toAdd);

        if (toAdd instanceof TokenValue) {
            this._verifyCompatible("TokenValue.add()", toAdd);
            return this.add(toAdd.value);
        }

        assert.parameterType("Token.add()", "toAdd", toAdd, Big);

        return new TokenValue(this.token, this.value.add(toAdd), this.decimals);
    }

    subtract(toSubtract) {
        assert.parameterIsDefined("TokenValue.subtract()", "toSubtract", toSubtract);

        if (toSubtract instanceof TokenValue) {
            this._verifyCompatible("TokenValue.subtract()", toSubtract);
            return this.subtract(toSubtract.value);
        }

        assert.parameterType("Token.subtract()", "toSubtract", toSubtract, Big);

        return new TokenValue(this.token, this.value.minus(toSubtract), this.decimals);
    }

    multiply(toMultiply) {
        assert.parameterIsDefined("TokenValue.multiply()", "toMultiply", toMultiply);

        if (toMultiply instanceof TokenValue) {
            this._verifyCompatible("TokenValue.multiply()", toMultiply);
            return this.multiply(toMultiply.value);
        }

        assert.parameterType("Token.multiply()", "toMultiply", toMultiply, Big);

        return new TokenValue(this.token, this.value.times(toMultiply), this.decimals);
    }

    divide(toDivide) {
        assert.parameterIsDefined("TokenValue.divide()", "toDivide", toDivide);

        if (toDivide instanceof TokenValue) {
            this._verifyCompatible("TokenValue.divide()", toDivide);
            return this.divide(toDivide.value);
        }

        assert.parameterType("Token.divide()", "toDivide", toDivide, Big);

        return new TokenValue(this.token, this.value.div(toDivide), this.decimals);
    }

    _verifyCompatible(description, anotherTokenValue) {
        if (!this.token.equals(anotherTokenValue.token)) {
            throw new Error(`${description} parameter error - token ${anotherTokenValue.token} does not match ${this.token}`);
        }

        if (this.decimals !== anotherTokenValue.decimals) {
            throw new Error(`${description} parameter error - decimals ${anotherTokenValue.decimals} does not match ${this.decimals}`);
        }
    }

    toString() {
        const rounded = this.roundQuantity(this.value);
        return `« ${rounded} ${this.token.symbol} »`;
    }
}

function loadTokenCache() {
    const allTokens = [];
    for (const tokenData of serum.TOKEN_MINTS) {
        const token = new Token(tokenData.name, tokenData.address, TokenSource.SERUMJS);
        allTokens.push(token);
    }

    const unknownToken = new Token("«Unknown»", new solana.PublicKey(), TokenSource.SAMM);

    return new itemcache.ItemCache(Token, allTokens, unknownToken);
}

async function createTokenAccountTransaction(context, payer, splToken) {
    const account = new solana.Account();
    const transaction = solana.SystemProgram.createAccount({
        fromPubkey: payer.publicKey,
        newAccountPubkey: account.publicKey,
        lamports: await context.connection.getMinimumBalanceForRentExemption(SOLANA_SYSTEMPROGRAM_SPACE),
        space: SOLANA_SYSTEMPROGRAM_SPACE,
        programId: serum.TokenInstructions.TOKEN_PROGRAM_ID
    });
    transaction.add(
        serum.TokenInstructions.initializeAccount({
            account: account.publicKey,
            mint: new solana.PublicKey(splToken.address),
            owner: payer.publicKey
        })
    );

    const signers = [payer, account];
    return {
        account,
        transaction,
        signers
    };
}

module.exports = {
    SOL_DECIMALS,
    TOKEN_PROGRAM_ID,
    TokenAccount,
    TokenSource,
    Token,
    TokenValue,
    loadTokenCache,
    createTokenAccountTransaction
};