"use strict";

const assert = require("./assert.js");
const ctx = require("./context.js");
const formatter = require("../lib/formatter.js");
const log = require("../lib/log.js");
const order = require("../lib/order.js");
const serum = require("@project-serum/serum");
const solana = require("@solana/web3.js");

class OrderCanceller {
    constructor() {
        assert.notConstructingAbstractType(OrderCanceller, new.target);

        this.isOrderCanceller = true;
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    cancelOrders() {
        assert.throwOnAbstractCall(OrderCanceller, this.cancelOrders);
    }
}

class SimulationOrderCanceller extends OrderCanceller {
    constructor() {
        super();

        if (new.target === SimulationOrderCanceller) {
            Object.freeze(this);
        }
    }

    // eslint-disable-next-line class-methods-use-this
    cancelOrders(orders) {
        assert.parameterTypeIsArrayOfType("SimulationOrderCanceller.cancelOrders()", "orders", orders, order.Order);

        for (const orderToCancel of orders) {
            log.print("Simulation - not cancelling order:", formatter.order(orderToCancel));
        }

        return Promise.resolve();
    }
}

class AbstractMarketOrderCanceller extends OrderCanceller {
    constructor(context, market, owner) {
        super();

        assert.parameterType("AbstractMarketOrderCanceller()", "context", context, ctx.Context);
        assert.parameterType("AbstractMarketOrderCanceller()", "market", market, serum.Market);
        assert.parameterType("AbstractMarketOrderCanceller()", "owner", owner, solana.Account);

        this.context = context;
        this.market = market;
        this.owner = owner;
    }

    cancelOrders() {
        assert.throwOnAbstractCall(AbstractMarketOrderCanceller, this.cancelOrders);
    }
}

class AsyncOrderCanceller extends AbstractMarketOrderCanceller {
    constructor(context, market, owner) {
        super(context, market, owner);

        if (new.target === AsyncOrderCanceller) {
            Object.freeze(this);
        }
    }

    cancelOrders(orders) {
        assert.parameterTypeIsArrayOfType("AsyncOrderCanceller.cancelOrders()", "orders", orders, order.Order);

        const cancellations = [];
        for (const orderToCancel of orders) {
            log.print("Cancelling order:", formatter.order(orderToCancel));
            cancellations.push(this.market.typedCancelOrder(this.owner, orderToCancel));
        }

        return Promise.all(cancellations);
    }
}

class AsyncWaitingOrderCanceller extends AbstractMarketOrderCanceller {
    constructor(context, market, owner) {
        super(context, market, owner);

        if (new.target === AsyncWaitingOrderCanceller) {
            Object.freeze(this);
        }
    }

    cancelOrders(orders) {
        assert.parameterTypeIsArrayOfType("AsyncWaitingOrderCanceller.cancelOrders()", "orders", orders, order.Order);

        const cancellations = [];
        for (const orderToCancel of orders) {
            log.print("Cancelling order:", formatter.order(orderToCancel));
            cancellations.push(
                this.market.typedCancelOrder(this.owner, orderToCancel)
                    .then((signature) => {
                        log.print("Waiting on signature:", signature);
                        return this.context.connection.waitFor(signature);
                    })
            );
        }

        return Promise.all(cancellations);
    }
}

class SyncOrderCanceller extends AbstractMarketOrderCanceller {
    constructor(context, market, owner) {
        super(context, market, owner);

        if (new.target === SyncOrderCanceller) {
            Object.freeze(this);
        }
    }

    async cancelOrders(orders) {
        assert.parameterTypeIsArrayOfType("SyncOrderCanceller.cancelOrders()", "orders", orders, order.Order);

        for (const orderToCancel of orders) {
            log.print("Cancelling order:", formatter.order(orderToCancel));
            // Yes, we really do want an await in a loop. This is a synchronous cancel.
            // eslint-disable-next-line no-await-in-loop
            const signature = await this.market.typedCancelOrder(this.owner, orderToCancel);

            // Here too.
            // eslint-disable-next-line no-await-in-loop
            await this.context.connection.waitFor(signature);
            log.print("Cancellation confirmed:", signature);
        }
    }
}

function createOrderCanceller(context, market, owner) {
    if (context.simulate) {
        log.trace("Using SimulationOrderCanceller");
        return new SimulationOrderCanceller();
    } else if (context.asynchronous) {
        log.trace("Using AsyncOrderCanceller");
        return new AsyncOrderCanceller(context, market, owner);
    } else if (context.asynchronousWaiting) {
        log.trace("Using AsyncWaitingOrderCanceller");
        return new AsyncWaitingOrderCanceller(context, market, owner);
    }

    log.trace("Using SyncOrderCanceller");
    return new SyncOrderCanceller(context, market, owner);
}

module.exports = {
    OrderCanceller,
    SimulationOrderCanceller,
    AbstractMarketOrderCanceller,
    AsyncOrderCanceller,
    AsyncWaitingOrderCanceller,
    SyncOrderCanceller,
    createOrderCanceller
};