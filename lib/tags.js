"use strict";

const array = require("./array");
const assert = require("./assert");
const enums = require("./enums");

const COMPOUND_TAG_SEPARATOR = ":";
const DEFAULT_TAG_PROPERTY_NAME = "tags";

function _normaliseTagValue(tag) {
    return tag.trim().toUpperCase();
}

function _nowString() {
    return new Date().toISOString();
}

class Tag {
    constructor(name) {
        assert.parameterTypeIsString("Tag()", "name", name);

        this.isTag = true;

        this.name = _normaliseTagValue(name);

        Object.freeze(this);
    }

    get [Symbol.toStringTag]() {
        return `« ${this.name} »`;
    }

    get isCompound() {
        return this.name.indexOf(COMPOUND_TAG_SEPARATOR) > -1;
    }

    get parts() {
        return this.name.split(COMPOUND_TAG_SEPARATOR);
    }

    is(name) {
        return this.name === _normaliseTagValue(name);
    }

    hasPrefix(prefix) {
        assert.parameterTypeIsString("Tag.hasPrefix()", "prefix", prefix);

        return this.parts[0] === _normaliseTagValue(prefix);
    }

    equals(tag) {
        assert.parameterType("Tag()", "tag", tag, Tag);
        return this.name === tag.name;
    }

    toString() {
        return `« ${this.name} »`;
    }
}

class TagCollection {
    constructor() {
        this.isTagCollection = true;

        this._tags = [];
    }

    get length() {
        return this._tags.length;
    }

    get [Symbol.toStringTag]() {
        return this._tags.map((tag) => tag.toString()).join(", ");
    }

    allTagNames() {
        return this._tags.map((tag) => tag.name);
    }

    add(...tagStrings) {
        assert.parameterTypeIsArrayOfType("TagCollection.add()", "tagStrings", tagStrings, String);

        for (const tagString of tagStrings) {
            if (!this.has(tagString)) {
                const tag = new Tag(tagString);
                this._tags.push(tag);
            }
        }
    }

    remove(...tagStrings) {
        assert.parameterTypeIsArrayOfType("TagCollection.remove()", "tagStrings", tagStrings, String);

        for (const tagString of tagStrings) {
            const tag = new Tag(tagString);
            array.remove(this._tags, tag, "equals");
        }
    }

    has(...tagStrings) {
        assert.parameterTypeIsArrayOfType("TagCollection.has()", "tagStrings", tagStrings, String);

        if (!tagStrings || tagStrings.length === 0) {
            return false;
        }

        for (const tagString of tagStrings) {
            const tag = new Tag(tagString);
            const index = this._tags.findIndex((existing) => tag.equals(existing));
            if (index === -1) {
                return false;
            }
        }

        return true;
    }

    addFlags(obj) {
        const allSetFlags = [];
        for (const flag in obj) {
            if (Object.prototype.hasOwnProperty.call(obj, flag) && obj[flag]) {
                allSetFlags.push(`FLAG:${flag}`);
            }
        }

        this.add(...allSetFlags);
    }

    *[Symbol.iterator]() {
        yield* this._tags;
    }

    toString() {
        const allTags = this._tags.map((tag) => tag.toString()).join(", ");
        return `« Tags: ${allTags} »`;
    }
}

class Tagger {
    constructor() {
        this.isTagger = true;
    }

    tag() {
        assert.throwOnAbstractCall(Tagger, this.tag);
    }
}

class FunctionTagger extends Tagger {
    constructor(tagFunc) {
        super();
        assert.parameterType("FunctionTagger()", "tagFunc", tagFunc, Function);

        this.tag = tagFunc;
    }
}

function ensureTags(obj, propertyName = DEFAULT_TAG_PROPERTY_NAME) {
    assert.parameterTypeIsString("ensureTags()", "propertyName", propertyName);

    if (!obj[propertyName]) {
        obj[propertyName] = new TagCollection();
        const typeName = obj[Symbol.toStringTag];
        if (typeName) {
            obj[propertyName].add(`type:${typeName}`);
        }

        const timestamp = _nowString();
        obj[propertyName].add(`timestamp:${timestamp}`);
    }

    assert.objectType("ensureTagged()", propertyName, obj[propertyName], TagCollection);

    return obj[propertyName];
}

function addTag(obj, ...tag) {
    return ensureTags(obj).add(...tag);
}

function removeTag(obj, ...tag) {
    return ensureTags(obj).remove(...tag);
}

function hasTag(obj, ...tag) {
    return ensureTags(obj).has(...tag);
}

function allTagNames(obj) {
    return ensureTags(obj).allTagNames();
}

const CommonTagOperations = {
    mine(tagged, type) {
        const tags = ensureTags(tagged);
        tags.add("mine");
        if (type) {
            tags.add(`mine:${type}`);
        }
    },

    notMine(tagged, type) {
        const tags = ensureTags(tagged);
        if (type) {
            tags.add(`notmine:${type}`);
        }
    },

    source(tagged, source) {
        assert.parameterType("source()", "source", source, enums.EnumValue);

        const tags = ensureTags(tagged);
        tags.add(`source:${source.value}`);
    },

    timestamp(tagged, label) {
        const timestamp = _nowString();
        const tags = ensureTags(tagged);
        tags.add(`${label}:${timestamp}`);
    },

    createMineFilterOrNoOp(doWeFilter) {
        if (doWeFilter) {
            return (tagged) => hasTag(tagged, "mine");
        }

        return () => true;
    },

    isTagged(tagged) {
        return Boolean(tagged.tags);
    },

    isMine(tagged) {
        assert.isTrue(this.isTagged(tagged), `Parameter '${tagged}' has no tags.`);
        return ensureTags(tagged).hasTag(tagged, "mine");
    }
};

module.exports = {
    Tag,
    TagCollection,
    Tagger,
    FunctionTagger,
    ensureTags,
    addTag,
    removeTag,
    hasTag,
    allTagNames,
    CommonTagOperations
};