"use strict";

const assert = require("./assert.js");
const enums = require("./enums.js");
const event = require("./event.js");
const fill = require("./fill.js");
const itemcache = require("./itemcache.js");
const order = require("./order.js");
const request = require("./request.js");
const serum = require("@project-serum/serum");
const solana = require("@solana/web3.js");
const retry = require("./retry.js");
const spreads = require("./spreads.js");
const tags = require("./tags.js");
const token = require("./token.js");

const MarketSource = enums.createEnum(
    "MarketSource",
    [
        new enums.EnumValue("testing"),
        new enums.EnumValue("samm"),
        new enums.EnumValue("serumjs"),
        new enums.EnumValue("pool"),
        new enums.EnumValue("additionalmarketfile"),
        new enums.EnumValue("commandline")
    ]
);


class Market extends serum.Market {
    constructor(context, name, address, programId, source) {
        assert.parameterTypeIsContext("Market()", "context", context);
        assert.parameterTypeIsString("Market()", "name", name);
        assert.parameterType("Market()", "address", address, solana.PublicKey);
        assert.parameterType("Market()", "programId", programId, solana.PublicKey);
        assert.parameterTypeIsEnum("Market()", "source", source, MarketSource);

        const decoded = {
            ownAddress: address,
            accountFlags: {
                initialized: true,
                market: true
            }
        };
        // eslint-disable-next-line no-undefined
        super(decoded, undefined, undefined, {}, programId);

        this.isMarket = true;

        this.context = context;
        this.name = name;

        this.symbol = name.toUpperCase();
        const [base, quote] = this.symbol.split("/");
        this.base = base;
        this.quote = quote;
        this.retrier = new retry.Retrier(context.retries);

        tags.ensureTags(this);
        tags.CommonTagOperations.source(this, source);

        this.initialized = false;
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    async initialize() {
        if (!this.initialized) {
            const marketOptions = {confirmations: this.context.connection.confirmations};
            const found = await serum.Market.load(this.context.connection, this.address, marketOptions, this.programId);

            Object.assign(this, found);

            this.initialized = true;

            tags.CommonTagOperations.timestamp(this, "initialized");
        }
    }

    spreadFetcher() {
        return new spreads.MarketSpreadFetcher(this.context, this);
    }

    tag(item) {
        tags.ensureTags(item).add(`market:${this.symbol}`);
        this.context.tagger.tag(item);
        return item;
    }

    tagArray(itemArray) {
        return itemArray.map((item) => this.tag(item));
    }

    fetchSpread() {
        const spreadFetcher = this.spreadFetcher();
        return this.tag(spreadFetcher.fetchSpread());
    }

    typedLoadFills(...params) {
        return this.retrier.withRetries("Market.typedLoadFills()", async() => {
            const serumFills = await this.loadFills(this.context.connection, ...params);
            return this.tagArray(fill.Fill.fromSerumArray(serumFills));
        });
    }

    typedLoadEventQueue(...params) {
        return this.retrier.withRetries("Market.typedLoadEventQueue()", async() => {
            const serumEvents = await this.loadEventQueue(this.context.connection, ...params);
            return this.tagArray(event.Event.fromSerumArray(serumEvents));
        });
    }

    typedLoadRequestQueue(...params) {
        return this.retrier.withRetries("Market.typedLoadRequestQueue()", async() => {
            const serumRequests = await this.loadRequestQueue(this.context.connection, ...params);
            return this.tagArray(request.Request.fromSerumArray(serumRequests));
        });
    }

    typedFindBaseTokenAccountsForOwner(...params) {
        return this.retrier.withRetries("Market.typedFindBaseTokenAccountsForOwner()", async() => {
            const untypedTokenAccounts = await this.findBaseTokenAccountsForOwner(this.context.connection, ...params);
            return untypedTokenAccounts.map((untyped) => new token.TokenAccount(
                untyped.pubkey,
                untyped.account
            )).map((toTag) => {
                tags.ensureTags(toTag).add("BaseTokenAccount");
                this.tag(toTag);
                return toTag;
            });
        });
    }

    typedFindQuoteTokenAccountsForOwner(...params) {
        return this.retrier.withRetries("Market.typedFindQuoteTokenAccountsForOwner()", async() => {
            const untypedTokenAccounts = await this.findQuoteTokenAccountsForOwner(this.context.connection, ...params);
            return untypedTokenAccounts.map((untyped) => new token.TokenAccount(
                untyped.pubkey,
                untyped.account
            )).map((toTag) => {
                tags.ensureTags(toTag).add("QuoteTokenAccount");
                this.tag(toTag);
                return toTag;
            });
        });
    }

    typedFindOpenOrdersAccountsForOwner(...params) {
        return this.retrier.withRetries("Market.typedFindOpenOrdersAccountsForOwner()", async() => {
            const untyped = await this.findOpenOrdersAccountsForOwner(this.context.connection, ...params);
            return this.tagArray(untyped);
        });
    }

    typedLoadOrdersForOwner(...params) {
        return this.retrier.withRetries("Market.typedLoadOrdersForOwner()", async() => {
            const serumOrders = await this.loadOrdersForOwner(this.context.connection, ...params) || [];
            return this.tagArray(order.Order.fromSerumArray(serumOrders));
        });
    }

    typedLoadBids(...params) {
        return this.retrier.withRetries("Market.typedLoadBids()", async() => {
            const serumOrders = Array.from(await this.loadBids(this.context.connection, ...params));
            return this.tagArray(order.Order.fromSerumArray(serumOrders));
        });
    }

    typedLoadAsks(...params) {
        return this.retrier.withRetries("Market.typedLoadAsks()", async() => {
            const serumOrders = Array.from(await this.loadAsks(this.context.connection, ...params));
            return this.tagArray(order.Order.fromSerumArray(serumOrders));
        });
    }

    typedPlaceOrder(typedOrder) {
        assert.parameterType("Market.typedPlaceOrder()", "typedOrder", typedOrder, order.Order);

        return this.retrier.withRetries("Market.typedPlaceOrder()", () => {
            const untypedOrder = typedOrder.toSerumOrder();
            return this.placeOrder(this.context.connection, untypedOrder);
        });
    }

    typedCancelOrder(owner, typedOrder) {
        assert.parameterType("Market.typedCancelOrder()", "owner", owner, solana.Account);
        assert.parameterType("Market.typedCancelOrder()", "typedOrder", typedOrder, order.Order);

        return this.retrier.withRetries("Market.typedCancelOrder()", () => {
            const untypedOrder = typedOrder.toSerumOrder();
            return this.cancelOrder(this.context.connection, owner, untypedOrder);
        });
    }

    typedSettleFunds(owner, baseTokenAccount, quoteTokenAccount, openOrders) {
        assert.parameterType("Market.typedSettleFunds()", "owner", owner, solana.Account);
        assert.parameterType("Market.typedSettleFunds()", "baseTokenAccount", baseTokenAccount, token.TokenAccount);
        assert.parameterType("Market.typedSettleFunds()", "quoteTokenAccount", quoteTokenAccount, token.TokenAccount);
        assert.parameterType("Market.typedSettleFunds()", "openOrders", openOrders, serum.OpenOrders);

        return this.retrier.withRetries("Market.typedSettleFunds()", () => this.settleFunds(
            this.context.connection,
            owner,
            openOrders,
            baseTokenAccount.publicKey,
            quoteTokenAccount.publicKey
        ));
    }

    toString() {
        return `« Market ${this.name} at ${this.address} [${this.tags}] »`;
    }
}

class Markets {
    constructor(marketCache) {
        this.marketCache = marketCache;
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    // eslint-disable-next-line class-methods-use-this
    baseSymbolFromMarketName(name) {
        return name.split("/")[0];
    }

    // eslint-disable-next-line class-methods-use-this
    quoteSymbolFromMarketName(name) {
        return name.split("/")[1];
    }

    all() {
        return this.cache.items;
    }

    async byName(name) {
        const market = this.marketCache.bySymbol(name);
        await market.initialize();

        return market;
    }

    toString() {
        return `« Markets [Count: ${this.cache.items.length}] »`;
    }
}

function loadMarketCache(context) {
    assert.parameterTypeIsContext("loadMarketCache()", "context", context);

    const allDefinitions = serum.MARKETS.filter((market) => !market.deprecated);
    const allMarkets = allDefinitions.map(
        (def) => new Market(context, def.name, def.address, def.programId, MarketSource.SERUMJS)
    );

    return new itemcache.ItemCache(Market, allMarkets);
}

module.exports = {
    MarketSource,
    Market,
    Markets,
    loadMarketCache
};