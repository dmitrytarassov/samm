"use strict";

const array = require("./array");
const assert = require("./assert.js");
const ctx = require("./context.js");
const file = require("../lib/file.js");
const {Fill} = require("../lib/fill.js");
const log = require("../lib/log.js");
const {Order} = require("../lib/order.js");
const path = require("path");

const TO_PERCENTAGE_FACTOR = 100;
const TO_PERCENTAGE_DECIMAL_PLACES = 2;

class Auditor {
    constructor() {
        this.isAuditor = true;
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    fill() {
        assert.throwOnAbstractCall(Auditor, this.fill);
    }
}

class NotMakerWarningAuditor extends Auditor {
    // eslint-disable-next-line class-methods-use-this
    fill(fill, order) {
        assert.parameterType("NotMakerWarningAuditor()", "fill", fill, Fill);
        assert.parameterType("NotMakerWarningAuditor()", "order", order, Order);

        const {
            side,
            price
        } = fill;
        const {
            maker
        } = fill.eventFlags;
        const orderId = (fill.orderId || "").toString();
        const clientId = (fill.clientOrderId || "").toString();

        if (!maker) {
            log.warn(`Not maker on order ${orderId} [client ID: ${clientId}] - ${side} at ${price} instead of ${order?.price}`);
        }
    }
}

class CsvAuditor extends Auditor {
    constructor(context) {
        super();

        assert.parameterType("Auditor()", "context", context, ctx.Context);

        this.directory = context.auditLogDirectory;
        if (!file.exists(this.directory)) {
            file.makeDirectory(this.directory);
        }

        const marketName = context.currentMarketOrThrow.name;
        this.market = marketName.replace("/", "-");
        this.feePercentage = context.feeProportion.times(TO_PERCENTAGE_FACTOR)
            .toFixed(TO_PERCENTAGE_DECIMAL_PLACES);
        this.positionSizePercentage = context.positionSizeProportion.times(TO_PERCENTAGE_FACTOR)
            .toFixed(TO_PERCENTAGE_DECIMAL_PLACES);
    }

    fill(fill, order) {
        assert.parameterType("CsvAuditor()", "fill", fill, Fill);
        assert.parameterType("CsvAuditor()", "order", order, Order);

        const timestamp = new Date().toISOString();
        const today = timestamp.split("T")[0];
        const filename = `${this.market}-${today}.csv`;
        const filepath = path.join(this.directory, filename);
        log.trace(`Writing CSV to ${filepath}`);
        if (!file.exists(filepath)) {
            file.appendFile(
                filepath,
                "Timestamp,Market,Fee%,Position Size%,Order ID,Client ID,Side,Maker,Price,Size,Fee,Tags\n"
            );
        }

        const {
            price,
            size,
            feeCost
        } = fill;
        const {
            maker
        } = fill.eventFlags;
        const orderId = (fill.orderId || "").toFixed(0);
        const side = fill.side.name.toUpperCase();

        // Note - it"s clientOrderId here, and it"s clientId in Order.
        const clientId = (fill.clientOrderId || "").toFixed(0);

        const tags = fill.tags.toString();

        file.appendFile(filepath, `${timestamp},${this.market},${this.feePercentage},${this.positionSizePercentage},${orderId},${clientId},${side},${maker},${price},${size},${feeCost},"${tags}"\n`);
    }
}

function createCompoundAuditor(...auditors) {
    return new array.CompoundItem(Auditor, "fill", ...auditors);
}

module.exports = {
    Auditor,
    NotMakerWarningAuditor,
    CsvAuditor,
    createCompoundAuditor
};