"use strict";

const fs = require("fs");
const os = require("os");

// I am not a fan of 'fs' and having to trap errors here.
function isDirectory(name) {
    try {
        return fs.statSync(name).isDirectory();
    } catch (err) {
        return false;
    }
}

// I am not a fan of 'fs' and having to trap errors here.
function isFile(name) {
    try {
        return fs.statSync(name).isFile();
    } catch (err) {
        return false;
    }
}

// I am not a fan of 'fs' and having to trap errors here.
function isReadable(name) {
    try {
        fs.accessSync(name, fs.constants.R_OK);
        return true;
    } catch (err) {
        return false;
    }
}

// I am not a fan of 'fs' and having to trap errors here.
function isWritable(name) {
    try {
        fs.accessSync(name, fs.constants.W_OK);
        return true;
    } catch (err) {
        return false;
    }
}

function ensureFileIsNotDirectory(filename) {
    if (isDirectory(filename)) {
        throw new Error(`${filename} exists but is a directory. Is this a docker volume mapping problem?

When running in docker, files that are mapped as volumes are automatically created as directories if that file doesn't exist before the docker command is run.

If this is the case, you may be best removing the directory, creating an empty file, and then running create-wallet again. Do check that the directory is empty before deleting it!

Recommended actions: Delete the directory, touch the file '${filename}', and re-run the command. For example, for the create-wallet command:
  $ rmdir /var/sammd/wallet.json
  $ touch /var/sammd/wallet.json
  $ /var/sammd/bin/samm create-wallet --overwrite
`);
    }
}

function exists(filename) {
    return fs.existsSync(filename);
}

function readFile(filename) {
    ensureFileIsNotDirectory(filename);

    if (!fs.existsSync(filename)) {
        throw new Error(`File ${filename} does not exist`);
    }

    if (!isFile(filename)) {
        throw new Error(`File ${filename} is not a file`);
    }

    if (!isReadable(filename)) {
        const user = os.userInfo();
        throw new Error(`File ${filename} exists but is not readable by user '${user.username}' (ID ${user.uid}:${user.gid}).`);
    }

    return fs.readFileSync(filename);
}

function writeFile(filename, data, overwrite) {
    ensureFileIsNotDirectory(filename);

    if (isFile(filename)) {
        if (!isWritable(filename)) {
            const user = os.userInfo();
            throw new Error(`File ${filename} exists but is not writable by user '${user.username}' (ID ${user.uid}:${user.gid}).`);
        }

        if (!overwrite) {
            throw new Error(`File ${filename} exists and no overwrite parameter specified.  If you want to overwrite this file, try adding --overwrite to the command line.`);
        }
    }

    return fs.writeFileSync(filename, data);
}

function appendFile(filename, data) {
    ensureFileIsNotDirectory(filename);

    if (isFile(filename)) {
        if (!isWritable(filename)) {
            const user = os.userInfo();
            throw new Error(`File ${filename} exists but is not writable by user '${user.username}' (ID ${user.uid}:${user.gid}).`);
        }
    }

    return fs.appendFileSync(filename, data);
}

function makeDirectory(directoryName) {
    return fs.mkdirSync(directoryName);
}

module.exports = {
    isDirectory,
    isFile,
    isReadable,
    isWritable,
    exists,
    readFile,
    writeFile,
    appendFile,
    makeDirectory
};