"use strict";

const assert = require("./assert.js");
const balances = require("./balances.js");
const ctx = require("./context.js");
const layouts = require("./layouts.js");
const log = require("./log.js");
const numbers = require("./numbers.js");
const solana = require("@solana/web3.js");
const tags = require("./tags.js");
const token = require("./token.js");

const CSV_HEADER_ELEMENTS = [
    "Name",
    "Total Supply",
    "Base",
    "Base Pool",
    "Quote",
    "Quote Pool",
    "My Pool Tokens",
    "My Fraction Of Pool",
    "My Percentage Of Pool",
    "My Notional Base Tokens",
    "My Notional Quote Tokens"
];

const CSV_HEADER = CSV_HEADER_ELEMENTS.join(",");
const CSV_DECIMAL_PLACES = 8;

class TokenSwapAccountState {
    static csvHeader() {
        return CSV_HEADER;
    }

    constructor(
        name,
        baseToken,
        baseTokenAddress,
        quoteToken,
        quoteTokenAddress,
        supply,
        poolAddress,
        tokenBalances,
        owner,
        ownedBalance
    ) {
        this.name = name;
        this.baseToken = baseToken;
        this.quoteToken = quoteToken;
        this.poolAddress = poolAddress;
        this.baseTokenAddress = baseTokenAddress;
        this.quoteTokenAddress = quoteTokenAddress;
        this.supply = supply;
        this.tokenBalances = tokenBalances;
        this.owner = owner;
        this.ownedBalance = ownedBalance;
        this.ownedFraction = ownedBalance.value.div(supply.value);
        this.ownedNotionalBase = tokenBalances.base.value.times(this.ownedFraction);
        this.ownedNotionalQuote = tokenBalances.quote.value.times(this.ownedFraction);

        tags.ensureTags(this);
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    toCsv() {
        const values = [
            this.name,
            this.supply.value.toString(),
            this.baseToken.symbol,
            this.tokenBalances.base.value.toString(),
            this.quoteToken.symbol,
            this.tokenBalances.quote.value.toString(),
            this.ownedBalance.value.toString(),
            this.ownedFraction.toString(),
            numbers.toPercentageString(this.ownedFraction, CSV_DECIMAL_PLACES),
            this.ownedNotionalBase.toFixed(CSV_DECIMAL_PLACES),
            this.ownedNotionalQuote.toFixed(CSV_DECIMAL_PLACES)
        ];

        return values.join(",");
    }
}

class TokenSwap {
    constructor(raw, context) {
        assert.parameterType("TokenSwap()", "context", context, ctx.Context);

        this.context = context;

        this.isTokenSwap = true;

        this.isInitialized = raw.isInitialized;
        this.nonce = raw.nonce;
        this.curveType = raw.curveType;
        this.tokenProgramId = new solana.PublicKey(raw.tokenProgramId);
        this.tokenAccountA = new solana.PublicKey(raw.tokenAccountA);
        this.tokenAccountB = new solana.PublicKey(raw.tokenAccountB);
        this.tokenPool = new solana.PublicKey(raw.tokenPool);
        this.mintA = new solana.PublicKey(raw.mintA);
        this.mintB = new solana.PublicKey(raw.mintB);
        this.feeAccount = new solana.PublicKey(raw.feeAccount);
        this.tradeFeeNumerator = numbers.createBigFromBNBuffer(raw.tradeFeeNumerator);
        this.tradeFeeDenominator = numbers.createBigFromBNBuffer(raw.tradeFeeDenominator);
        this.ownerTradeFeeNumerator = numbers.createBigFromBNBuffer(raw.ownerTradeFeeNumerator);
        this.ownerTradeFeeDenominator = numbers.createBigFromBNBuffer(raw.ownerTradeFeeDenominator);
        this.ownerWithdrawFeeNumerator = numbers.createBigFromBNBuffer(raw.ownerWithdrawFeeNumerator);
        this.ownerWithdrawFeeDenominator = numbers.createBigFromBNBuffer(raw.ownerWithdrawFeeDenominator);

        tags.ensureTags(this);
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    async supply() {
        const tokenSupply = await this.context.connection.getTokenSupply(this.tokenPool);
        const value = numbers.createBigFromBalanceResult(tokenSupply);
        return new token.TokenValue(this.token, value, tokenSupply.value.decimals);
    }

    get base() {
        return this.context.tokenCache.byAddressOrUnknown(this.mintA);
    }

    get quote() {
        return this.context.tokenCache.byAddressOrUnknown(this.mintB);
    }

    get token() {
        return new token.Token(`${this.base.symbol}/${this.quote.symbol} Pool Token`, this.tokenPool, token.TokenSource.POOL);
    }

    async balances() {
        const {connection} = this.context;
        async function fetchBalance(tokenMetadata, accountPublicKey) {
            const accountBalance = await connection.getTokenAccountBalance(accountPublicKey);
            const tokens = numbers.createBigFromNumberAndDecimals(
                accountBalance.value.amount, accountBalance.value.decimals
            );

            return new token.TokenValue(tokenMetadata, tokens, accountBalance.value.decimals);
        }

        const baseTokenValue = await fetchBalance(this.base, this.tokenAccountA);
        const quoteTokenValue = await fetchBalance(this.quote, this.tokenAccountB);

        return new balances.BalancePair(baseTokenValue, quoteTokenValue);
    }
}

async function loadSwaps(context, rootAddress) {
    assert.parameterType("loadSwaps()", "context", context, ctx.Context);
    assert.parameterType("loadSwaps()", "rootAddress", rootAddress, solana.PublicKey);

    const swaps = [];
    const rawSwaps = await context.connection.getProgramAccounts(rootAddress);
    for (const rawSwap of rawSwaps) {
        const {data} = rawSwap.account;
        if (data.length === layouts.TokenSwapLayout.span) {
            const decoded = layouts.TokenSwapLayout.decode(data);
            const swap = new TokenSwap(decoded, context);
            swaps.push(swap);
        } else {
            log.warn(`Unknown token swap layout for PublicKey: ${rawSwap.pubkey}`);
        }
    }

    return swaps;
}

module.exports = {
    TokenSwapAccountState,
    TokenSwap,
    loadSwaps
};