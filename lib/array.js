"use strict";

const assert = require("./assert");
const log = require("./log");

function _buildEqualityTestFunction(equalityTest, entry) {
    if (equalityTest) {
        return (existing) => entry[equalityTest](existing);
    }

    return (existing) => existing === entry;
}

function find(arr, entry, equalityTest) {
    assert.parameterTypeIsArray("has()", "arr", arr);

    const finder = _buildEqualityTestFunction(equalityTest, entry);
    return arr.findIndex(finder);
}

function has(arr, entry, equalityTest) {
    assert.parameterTypeIsArray("has()", "arr", arr);

    return find(arr, entry, equalityTest) > -1;
}

function hasAllMembersOrderDependent(arr1, arr2, equalityTest) {
    assert.parameterTypeIsArray("has()", "arr1", arr1);
    assert.parameterTypeIsArray("has()", "arr2", arr2);

    if (arr1.length !== arr2.length) {
        return false;
    }

    for (let index = 0; index < arr1.length; index += 1) {
        const item1 = arr1[index];
        const item2 = arr2[index];
        const tester = _buildEqualityTestFunction(equalityTest, item1);
        if (!tester(item2)) {
            return false;
        }
    }

    return true;
}

function hasAllMembersOrderIndependent(arr1, arr2, equalityTest) {
    assert.parameterTypeIsArray("has()", "arr1", arr1);
    assert.parameterTypeIsArray("has()", "arr2", arr2);

    return arr1.every((item) => has(arr2, item, equalityTest))
        && arr2.every((item) => has(arr1, item, equalityTest));
}

function insertAtIndex(arr, index, entry) {
    assert.parameterTypeIsArray("insertAtIndex()", "arr", arr);
    assert.isTrue(index <= arr.length, `insertAtIndex() - index ${index} must be less than array length ${arr.length}`);

    if (index === arr.length) {
        arr.push(entry);
    } else {
        arr.splice(index, 0, entry);
    }
}

function insertAtHead(arr, entry) {
    insertAtIndex(arr, 0, entry);
}

function removeIndex(arr, index) {
    assert.parameterTypeIsArray("removeIndex()", "arr", arr);
    assert.isTrue(index < arr.length, `removeIndex() - index ${index} must be less than array length ${arr.length}`);

    if (index > -1) {
        arr.splice(index, 1);
    }
}

function remove(arr, entry, equalityTest) {
    assert.parameterTypeIsArray("remove()", "arr", arr);

    const index = find(arr, entry, equalityTest);
    removeIndex(arr, index);
}

class CompoundItem {
    constructor(type, funcName, ...items) {
        assert.parameterType("CompoundItem()", "type", type, Function);
        assert.parameterTypeIsString("CompoundItem()", "funcName", funcName);
        assert.parameterTypeIsArrayOfType("CompoundItem()", "items", items, type);

        this.type = type;
        this.items = [...items];

        this[funcName] = function compoundCall(...params) {
            for (const item of this.items) {
                try {
                    item[funcName](...params);
                } catch (ex) {
                    log.warn(`Exception while calling '${funcName}' (in item ${item?.constructor?.name}): ${ex}`, ex);
                }
            }
        };
    }

    add(item) {
        assert.parameterType("CompoundItem.add()", "item", item, this.type);

        this.items.push(item);
    }

    remove(item) {
        assert.parameterType("CompoundItem.remove()", "item", item, this.type);

        remove(this.items, item);
    }
}

module.exports = {
    find,
    has,
    hasAllMembersOrderDependent,
    hasAllMembersOrderIndependent,
    insertAtIndex,
    insertAtHead,
    removeIndex,
    remove,
    CompoundItem
};