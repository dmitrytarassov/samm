"use strict";

const numbers = require("./numbers.js");
const tags = require("./tags.js");

// A request from Serum usually looks something like as an object:
// {
//     requestFlags: {
//       newOrder: true,
//       cancelOrder: false,
//       bid: true,
//       postOnly: true,
//       ioc: false
//     },
//     openOrdersSlot: 2,
//     feeTier: 0,
//     maxBaseSizeOrCancelId: <BN: 1f4>,
//     nativeQuoteQuantityLocked: <BN: bcd0162>,
//     orderId: <BN: 9a56ffffffffffd1b7cd>,
//     openOrders: PublicKey {
//       _bn: <BN: 21688380dd834b4b153cc55baf7b9b4d3189fc70e57e6ae225a8736180fff7e1>
//     },
//     clientOrderId: <BN: 1e3>
// }
// And as JSON:
// {
//     "requestFlags": {
//         "newOrder": true,
//         "cancelOrder": false,
//         "bid": true,
//         "postOnly": true,
//         "ioc": false
//     },
//     "openOrdersSlot": 2,
//     "feeTier": 0,
//     "maxBaseSizeOrCancelId": "01f4",
//     "nativeQuoteQuantityLocked": "0bcd0162",
//     "orderId": "9a56ffffffffffd1b7cd",
//     "openOrders": {
//         "_bn": "21688380dd834b4b153cc55baf7b9b4d3189fc70e57e6ae225a8736180fff7e1"
//     },
//     "clientOrderId": "01e3"
// }
class Request {
    static fromSerum(untyped) {
        const clone = {
            ...untyped
        };

        clone.orderId = numbers.convertBNToBigOrUndefined(untyped.orderId);
        clone.clientOrderId = numbers.convertBNToBigOrUndefined(untyped.clientOrderId);
        clone.maxBaseSizeOrCancelId = numbers.convertBNToBigOrUndefined(untyped.maxBaseSizeOrCancelId);
        clone.nativeQuoteQuantityLocked = numbers.convertBNToBigOrUndefined(untyped.nativeQuoteQuantityLocked);

        const keys = [
            clone.openOrders.toString(),
            clone.orderId.toFixed(0),
            clone.nativeQuoteQuantityLocked.toFixed(0)
        ];
        clone.key = keys.join("+");

        return new Request(clone);
    }

    static fromSerumArray(untypeds) {
        return untypeds.map((untyped) => Request.fromSerum(untyped));
    }

    constructor(
        untyped
    ) {
        Object.assign(this, untyped);

        this.isRequest = true;

        tags.ensureTags(this);
        this.tags.addFlags(this.requestFlags);

        Object.freeze(this);
    }

    get [Symbol.toStringTag]() {
        return this.constructor?.name;
    }

    toString() {
        return `« Request ID ${this.orderId} [${this.clientOrderId}] »`;
    }
}

module.exports = {
    Request
};