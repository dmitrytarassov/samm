#! /usr/bin/env node

"use strict";

const accounttracker = require("../lib/accounttracker.js");
const Big = require("big.js");
const balances = require("../lib/balances.js");
const ctx = require("../lib/context.js");
const formatter = require("../lib/formatter.js");
const log = require("../lib/log.js");
const token = require("../lib/token.js");

async function main() {
    const context = await ctx.loadContext(process.argv);
    const owner = context.loadAccount();
    const sols = await context.connection.getBalance(owner.publicKey);
    const solToken = context.tokenCache.bySymbol("SOL");
    const solBalance = token.TokenValue.createFromQuantity(solToken, new Big(sols), token.SOL_DECIMALS);

    if (context.currentMarket) {
        const market = context.currentMarket;
        const accountTracker = await accounttracker.createAccountTracker(context, market, owner);
        const fetcher = balances.createFetcher(context, market, owner, accountTracker);
        const bal = await fetcher.fetchBalance();
        const spread = await market.fetchSpread();
        const report = formatter.balancesReport(solToken, solBalance, bal, spread);
        report.map((line) => log.print(line));
    } else {
        log.print(formatter.balance(solBalance));
    }
}

main().catch((ex) => log.critical(ex));