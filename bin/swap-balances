#! /usr/bin/env node

"use strict";

const ctx = require("../lib/context.js");
const log = require("../lib/log.js");
const formatter = require("../lib/formatter.js");
const numbers = require("../lib/numbers.js");
const solana = require("@solana/web3.js");
const token = require("../lib/token.js");
const tokenswap = require("../lib/tokenswap.js");

async function sumBalances(context, accounts, swap) {
    const tokenValues = [];
    for (const account of accounts) {
        // eslint-disable-next-line no-await-in-loop
        const balanceResult = await context.connection.getTokenAccountBalance(account.publicKey);
        const balance = numbers.createBigFromBalanceResult(balanceResult);
        const tokenValue = new token.TokenValue(swap.token, balance, balanceResult.value.decimals);
        tokenValues.push(tokenValue);
    }

    const sum = (accumulator, balance) => accumulator.plus(balance.value);
    const total = tokenValues.reduce(sum, numbers.Zero);
    return new token.TokenValue(swap.token, total, tokenValues[0].decimals);
}

async function formatSwapBalances(context, owner, allAccounts, swap, format) {
    const accounts = allAccounts.value.filter((acc) => acc.account.mint.equals(swap.tokenPool));
    if (accounts.length === 0) {
        return "";
    }

    const myPoolBalance = await sumBalances(context, accounts, swap);
    if (myPoolBalance.value.gt(numbers.Zero)) {
        const tokenBalances = await swap.balances();
        const supply = await swap.supply();
        const state = new tokenswap.TokenSwapAccountState(
            `${swap.base.symbol}/${swap.quote.symbol}`,
            swap.base,
            swap.tokenAccountA,
            swap.quote,
            swap.tokenAccountB,
            supply,
            swap.tokenPool,
            tokenBalances,
            owner,
            myPoolBalance
        );

        return format(state);
    }

    return "";
}

async function main() {
    const context = await ctx.loadContext(process.argv);

    const owner = context.loadAccount();

    const accounts = await context.connection.getTokenAccountsByOwner(owner.publicKey, {
        programId: token.TOKEN_PROGRAM_ID
    });

    const poolProgramId = new solana.PublicKey(context.poolRoot);
    const swaps = await tokenswap.loadSwaps(context, poolProgramId);
    const output = formatter.makeOutput(context, formatter.tokenSwapAccountState);

    if (context.csv) {
        output.printer(tokenswap.TokenSwapAccountState.csvHeader());
    }

    for (const swap of swaps) {
        // eslint-disable-next-line no-await-in-loop
        const toPrint = await formatSwapBalances(context, owner, accounts, swap, output.formatter);
        output.printer(toPrint);
    }
}

main().catch((ex) => log.error(ex));