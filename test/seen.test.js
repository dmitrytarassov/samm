"use strict";

const Big = require("big.js");
const {
    expect
} = require("chai");
const log = require("../lib/log.js");
const seen = require("../lib/seen.js");

describe("Seen", () => {
    log.setLevels({
        trace: false,
        print: false
    });
    describe("Seen", () => {
        it("Constructor should fail with no parameters", () => {
            const shouldThrow = () => new seen.Seen();
            expect(shouldThrow).to.throw();
        });
        it("Constructor should fail with undefined parameter", () => {
            // eslint-disable-next-line no-undefined
            const shouldThrow = () => new seen.Seen(undefined);
            expect(shouldThrow).to.throw();
        });
        it("Constructor should fail with non-number parameter", () => {
            const shouldThrow = () => new seen.Seen("x");
            expect(shouldThrow).to.throw();
        });
        it("Constructor should successfully construct with valid parameters", () => {
            const actual = new seen.Seen(5);
            expect(actual.constructor.name).to.equal("Seen");
        });
        it("markSeen should succeed when called with undefined", () => {
            const actual = new seen.Seen(5);

            // eslint-disable-next-line no-undefined
            actual.markSeen(undefined);
        });
        it("alreadySeen should return false when nothing seen", () => {
            const actual = new seen.Seen(5);
            const id = new Big(0);
            expect(actual.alreadySeen(id)).to.be.false;
        });
        it("alreadySeen should return false when called with undefined", () => {
            const actual = new seen.Seen(5);
            const id = new Big(0);
            actual.markSeen(id);

            // eslint-disable-next-line no-undefined
            expect(actual.alreadySeen(undefined)).to.be.false;
        });
        it("alreadySeen should return true when ID seen", () => {
            const actual = new seen.Seen(5);
            const id = new Big(0);
            actual.markSeen(id);
            expect(actual.alreadySeen(id)).to.be.true;
        });
        it("alreadySeen should return true when ID seen, using different object but same value", () => {
            const actual = new seen.Seen(5);
            actual.markSeen(new Big(0));
            expect(actual.alreadySeen(new Big(0))).to.be.true;
        });
        it("alreadySeen should return false when seen IDs greater than max", () => {
            const actual = new seen.Seen(5);
            actual.markSeen(new Big(0));
            actual.markSeen(new Big(1));
            actual.markSeen(new Big(2));
            actual.markSeen(new Big(3));
            actual.markSeen(new Big(4));
            actual.markSeen(new Big(5));
            expect(actual.alreadySeen(new Big(0))).to.be.false;
            expect(actual.alreadySeen(new Big(1))).to.be.true;
            expect(actual.alreadySeen(new Big(2))).to.be.true;
            expect(actual.alreadySeen(new Big(3))).to.be.true;
            expect(actual.alreadySeen(new Big(4))).to.be.true;
            expect(actual.alreadySeen(new Big(5))).to.be.true;
        });
        it("alreadySeen should return false when seen IDs greater than max with strings", () => {
            const actual = new seen.Seen(5);
            actual.markSeen("a");
            actual.markSeen("b");
            actual.markSeen("c");
            actual.markSeen("d");
            actual.markSeen("e");
            actual.markSeen("f");
            expect(actual.alreadySeen("a")).to.be.false;
            expect(actual.alreadySeen("b")).to.be.true;
            expect(actual.alreadySeen("c")).to.be.true;
            expect(actual.alreadySeen("d")).to.be.true;
            expect(actual.alreadySeen("e")).to.be.true;
            expect(actual.alreadySeen("f")).to.be.true;
        });
    });
});