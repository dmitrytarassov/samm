"use strict";

const Big = require("big.js");
const {
    expect
} = require("chai");
const fakes = require("./fakes.js");
const log = require("../lib/log.js");
const numbers = require("../lib/numbers.js");
const {
    TokenAccount,
    TokenSource,
    Token,
    TokenValue
} = require("../lib/token.js");

describe("Tokens", () => {
    log.setLevels({
        trace: false,
        print: false
    });

    const FAKE_TOKEN_ADDRESS = fakes.randomPublicKey();
    const ALT_FAKE_TOKEN_ADDRESS = fakes.randomPublicKey();

    describe("TokenAccount", () => {
        it("Constructor should successfully construct with valid parameters", () => {
            const publicKey = fakes.randomPublicKey();
            const account = fakes.tokenAccountOwner();
            const tokenAccount = new TokenAccount(publicKey, account);
            expect(tokenAccount.publicKey).to.equal(publicKey);
            expect(tokenAccount.account).to.equal(account);
        });

        it("Constructor should throw with invalid public key", () => {
            const account = fakes.tokenAccountOwner();
            const shouldThrow = () => new TokenAccount("invalid", account);
            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid account", () => {
            const publicKey = fakes.randomPublicKey();
            const shouldThrow = () => new TokenAccount(publicKey, "invalid");
            expect(shouldThrow).to.throw();
        });

        it("TokenAccount should equal itself", () => {
            const publicKey = fakes.randomPublicKey();
            const account = fakes.tokenAccountOwner();
            const tokenAccount = new TokenAccount(publicKey, account);
            expect(tokenAccount.equals(tokenAccount)).to.be.true;
        });

        it("TokenAccount should equal token account with same values", () => {
            const publicKey = fakes.randomPublicKey();
            const account = fakes.tokenAccountOwner();
            const tokenAccount1 = new TokenAccount(publicKey, account);
            const tokenAccount2 = new TokenAccount(publicKey, account);
            expect(tokenAccount1.equals(tokenAccount2)).to.be.true;
        });

        it("TokenAccount should equal clone", () => {
            const publicKey = fakes.randomPublicKey();
            const account = fakes.tokenAccountOwner();
            const tokenAccount = new TokenAccount(publicKey, account);
            const clone = {
                ...tokenAccount
            };
            expect(tokenAccount.equals(clone)).to.be.true;
        });

        it("TokenAccount should not equal undefined", () => {
            const publicKey = fakes.randomPublicKey();
            const account = fakes.tokenAccountOwner();
            const tokenAccount = new TokenAccount(publicKey, account);

            // eslint-disable-next-line no-undefined
            expect(tokenAccount.equals(undefined)).to.be.false;
        });

        it("TokenAccount should not equal token with different public key", () => {
            const publicKey1 = fakes.randomPublicKey();
            const publicKey2 = fakes.randomPublicKey();
            const account = fakes.tokenAccountOwner();
            const tokenAccount1 = new TokenAccount(publicKey1, account);
            const tokenAccount2 = new TokenAccount(publicKey2, account);
            expect(tokenAccount1.equals(tokenAccount2)).to.be.false;
        });

        it("TokenAccount should not equal token with different account", () => {
            const publicKey = fakes.randomPublicKey();
            const account1 = fakes.tokenAccountOwner();
            const account2 = fakes.tokenAccountOwner();
            const tokenAccount1 = new TokenAccount(publicKey, account1);
            const tokenAccount2 = new TokenAccount(publicKey, account2);
            expect(tokenAccount1.equals(tokenAccount2)).to.be.false;
        });
    });

    describe("Token", () => {
        it("Constructor should successfully construct with valid parameters", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            expect(token.isToken).to.be.true;
            expect(token.symbol).to.equal("TOK");
            expect(token.address).to.equal(FAKE_TOKEN_ADDRESS, 5);
        });

        it("Constructor should throw with invalid symbol type", () => {
            const shouldThrow = () => new Token({}, FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid address type", () => {
            const shouldThrow = () => new Token("TOK", "invalid", TokenSource.TESTING);
            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid source", () => {
            const shouldThrow = () => new Token("TOK", FAKE_TOKEN_ADDRESS, "invalid");
            expect(shouldThrow).to.throw();
        });

        it("Token should equal itself", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            expect(token.equals(token)).to.be.true;
        });

        it("Token should equal token with same values", () => {
            const token1 = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const token2 = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            expect(token1.equals(token2)).to.be.true;
        });

        it("Token should equal clone", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const clone = {
                ...token
            };
            expect(token.equals(clone)).to.be.true;
        });

        it("Token should not equal undefined", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);

            // eslint-disable-next-line no-undefined
            expect(token.equals(undefined)).to.be.false;
        });

        it("Token should not equal token with different symbol", () => {
            const token1 = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const token2 = new Token("TOK2", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            expect(token1.equals(token2)).to.be.false;
        });

        it("Token should not equal token with different address", () => {
            const token1 = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const token2 = new Token("TOK", ALT_FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            expect(token1.equals(token2)).to.be.false;
        });

        it("Token should not equal token with different decimals", () => {
            const token1 = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const token2 = new Token("TOK", ALT_FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            expect(token1.equals(token2)).to.be.false;
        });

        it("Token should equal token with different source", () => {
            // NOTE: source isn't taken into account for equality.
            const token1 = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const token2 = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.SAMM);
            expect(token1.equals(token2)).to.be.true;
        });
    });

    describe("TokenValue", () => {
        it("Constructor should successfully construct with valid parameters", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue = new TokenValue(token, new Big(1), 9);
            expect(tokenValue.isTokenValue).to.be.true;
            expect(tokenValue.token).to.equal(token);
            expect(tokenValue.value.eq(new Big(1))).to.be.true;
            expect(tokenValue.decimals).to.equal(9);
        });

        it("Constructor should throw with invalid token type", () => {
            const shouldThrow = () => new TokenValue("invalid", new Big(10), 9);
            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid value type", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const shouldThrow = () => new TokenValue(token, "invalid", 9);
            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid decimals type", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const shouldThrow = () => new TokenValue(token, new Big(10), "invalid");
            expect(shouldThrow).to.throw();
        });

        it("Value should return correct value for simple 1", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue = new TokenValue(token, numbers.One, 9);
            expect(tokenValue.value.eq(numbers.One)).to.be.true;
        });

        it("Value should return correct value for 1.5", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue = new TokenValue(token, new Big(1.5), 9);
            expect(tokenValue.value.eq(new Big(1.5))).to.be.true;
        });

        it("Value should return correct value for 0.001", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue = new TokenValue(token, new Big(0.001), 9);
            expect(tokenValue.value.eq(new Big(0.001))).to.be.true;
        });

        it("TokenValue should equal itself", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue = new TokenValue(token, new Big(1000000), 9);
            expect(tokenValue.equals(tokenValue)).to.be.true;
        });

        it("TokenValue should equal TokenValue with same values", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue1 = new TokenValue(token, new Big(1000000), 9);
            const tokenValue2 = new TokenValue(token, new Big(1000000), 9);
            expect(tokenValue1.equals(tokenValue2)).to.be.true;
        });

        it("TokenValue should equal clone", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue = new TokenValue(token, new Big(1000000), 9);
            const clone = {
                ...tokenValue
            };
            expect(tokenValue.equals(clone)).to.be.true;
        });

        it("TokenValue should not equal undefined", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue = new TokenValue(token, new Big(1000000), 9);

            // eslint-disable-next-line no-undefined
            expect(tokenValue.equals(undefined)).to.be.false;
        });

        it("TokenValue should not equal TokenValue with different symbol", () => {
            const token1 = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue1 = new TokenValue(token1, new Big(1000000), 9);
            const token2 = new Token("TOK2", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue2 = new TokenValue(token2, new Big(1000000), 9);
            expect(tokenValue1.equals(tokenValue2)).to.be.false;
        });

        it("TokenValue should not equal TokenValue with different address", () => {
            const token1 = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue1 = new TokenValue(token1, new Big(1000000), 9);
            const token2 = new Token("TOK", ALT_FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue2 = new TokenValue(token2, new Big(1000000), 9);
            expect(tokenValue1.equals(tokenValue2)).to.be.false;
        });

        it("TokenValue should not equal TokenValue with different value", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue1 = new TokenValue(token, new Big(1000000), 9);
            const tokenValue2 = new TokenValue(token, new Big(2000000), 9);
            expect(tokenValue1.equals(tokenValue2)).to.be.false;
        });

        it("TokenValue should not equal TokenValue with different decimals", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue1 = new TokenValue(token, new Big(1000000), 9);
            const tokenValue2 = new TokenValue(token, new Big(1000000), 10);
            expect(tokenValue1.equals(tokenValue2)).to.be.false;
        });

        it("Adding undefined should throw", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue = new TokenValue(token, numbers.One, 9);
            expect(tokenValue.value.eq(numbers.One)).to.be.true;

            // eslint-disable-next-line no-undefined
            expect(() => tokenValue.add(undefined)).to.throw();
        });

        it("Adding 1 should increase value by 1", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue = new TokenValue(token, numbers.One, 9);
            const addition = new TokenValue(token, numbers.One, 9);
            const result = tokenValue.add(addition);
            expect(result.value.eq(new Big(2))).to.be.true;
        });

        it("Adding 1.5 should increase value by 1.5", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue = new TokenValue(token, new Big(1), 9);
            const addition = new TokenValue(token, new Big(1.5), 9);
            const result = tokenValue.add(addition);
            expect(result.value.eq(new Big(2.5))).to.be.true;
        });

        it("Adding 1 should fail if token does not match", () => {
            const token1 = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue = new TokenValue(token1, new Big(1000000000), 9);
            const token2 = new Token("TOK2", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const addition = new TokenValue(token2, new Big(1000000000), 9);
            expect(() => tokenValue.add(addition)).to.throw();
        });

        it("Adding 1 should fail if decimals does not match", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue = new TokenValue(token, new Big(1000000000), 9);
            const addition = new TokenValue(token, new Big(1000000000), 10);
            expect(() => tokenValue.add(addition)).to.throw();
        });

        it("Subtracting undefined should throw", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue = new TokenValue(token, numbers.One, 9);
            expect(tokenValue.value.eq(numbers.One)).to.be.true;

            // eslint-disable-next-line no-undefined
            expect(() => tokenValue.subtract(undefined)).to.throw();
        });

        it("Subtracting 1 should decrease value by 1", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue = new TokenValue(token, numbers.One, 9);
            const subtraction = new TokenValue(token, numbers.One, 9);
            const result = tokenValue.subtract(subtraction);
            expect(result.value.eq(numbers.Zero)).to.be.true;
        });

        it("Subtracting 1.5 should decrease value by 1.5", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue = new TokenValue(token, new Big(1), 9);
            const subtraction = new TokenValue(token, new Big(1.5), 9);
            const result = tokenValue.subtract(subtraction);
            expect(result.value.eq(new Big(-0.5))).to.be.true;
        });

        it("Subtracting 1 should fail if token does not match", () => {
            const token1 = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue = new TokenValue(token1, new Big(1), 9);
            const token2 = new Token("TOK2", ALT_FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const subtraction = new TokenValue(token2, new Big(1), 9);
            expect(() => tokenValue.subtract(subtraction)).to.throw();
        });

        it("Subtracting 1 should fail if decimals does not match", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue = new TokenValue(token, new Big(1), 9);
            const subtraction = new TokenValue(token, new Big(1), 10);
            expect(() => tokenValue.subtract(subtraction)).to.throw();
        });

        it("Multiplying by undefined should throw", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue = new TokenValue(token, new Big(1), 9);
            expect(tokenValue.value.eq(new Big(1))).to.be.true;

            // eslint-disable-next-line no-undefined
            expect(() => tokenValue.multiply(undefined)).to.throw();
        });

        it("Multiplying 2 by 3 should give 6", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue = new TokenValue(token, new Big(2), 9);
            const multiplier = new TokenValue(token, new Big(3), 9);
            const result = tokenValue.multiply(multiplier);
            expect(result.value.eq(new Big(6))).to.be.true;
        });

        it("Dividing by undefined should throw", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue = new TokenValue(token, new Big(1), 9);
            expect(tokenValue.value.eq(new Big(1))).to.be.true;

            // eslint-disable-next-line no-undefined
            expect(() => tokenValue.divide(undefined)).to.throw();
        });

        it("Dividing 6 by 3 should give 2", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue = new TokenValue(token, new Big(6), 9);
            const divisor = new TokenValue(token, new Big(3), 9);
            const result = tokenValue.divide(divisor);
            expect(result.value.eq(new Big(2))).to.be.true;
        });

        it("createFromQuantity() should throw with invalid token type", () => {
            const shouldThrow = () => TokenValue.createFromQuantity("invalid", new Big(10), 9);
            expect(shouldThrow).to.throw();
        });

        it("createFromQuantity() should throw with invalid value type", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const shouldThrow = () => TokenValue.createFromQuantity(token, "invalid", 9);
            expect(shouldThrow).to.throw();
        });

        it("createFromQuantity() should throw with invalid decimals type", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const shouldThrow = () => TokenValue.createFromQuantity(token, new Big(10), "invalid");
            expect(shouldThrow).to.throw();
        });

        it("createFromQuantity() should calculate correct value", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue = TokenValue.createFromQuantity(token, new Big(1234567), 5);
            expect(tokenValue.value.eq(new Big("12.34567"))).to.be.true;
        });

        it("roundQuantity() should fail with invalid value", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue = new TokenValue(token, new Big(1000000), 5);
            const shouldThrow = () => tokenValue.roundQuantity("invalid");
            expect(shouldThrow).to.throw();
        });

        it("roundQuantity() should round down to correct decimal places", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue = new TokenValue(token, new Big(1000000), 5);
            expect(tokenValue.roundQuantity(new Big("0.1111111111")).eq(new Big("0.11111"))).to.be.true;
        });

        it("roundQuantity() should round up to correct decimal places", () => {
            const token = new Token("TOK", FAKE_TOKEN_ADDRESS, TokenSource.TESTING);
            const tokenValue = new TokenValue(token, new Big(1000000), 5);
            expect(tokenValue.roundQuantity(new Big("0.123456789")).eq(new Big("0.12346"))).to.be.true;
        });
    });
});