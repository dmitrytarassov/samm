"use strict";

const Big = require("big.js");
const constantproduct = require("../lib/constantproduct.js");
const {
    expect
} = require("chai");
const log = require("../lib/log.js");
const solana = require("@solana/web3.js");
const {
    TokenSource,
    Token,
    TokenValue
} = require("../lib/token.js");

describe("ConstantProduct", () => {
    log.setLevels({
        trace: false,
        print: false
    });

    // Some tests (usually totalCost tests since that value isn"t rounded) involve comparing
    // with results from a spreadsheet, so we can come across some differences in precision.
    //
    // If a and b are nearly equal though, a/b should be nearly 1, so
    // (a-b/b)-1 should be nearly zero.
    function roughlyEqual(big1, big2) {
        // console.log(big1.toString(), big2.toString(), big1.sub(big2).div(big2).abs().toString())
        return big1.sub(big2).div(big2)
            .abs()
            .lt(new Big("0.00000000001"));
    }

    describe("ConstantProductCalculator", () => {
        const fee = new Big("0.001");

        const BTC_TOKEN_ADDRESS = new solana.PublicKey("9n4nbM75f5Ui33ZbPYXn59EwSgE8CGsHtAeTH5YFeJ9E");
        const wbtcToken = new Token("WBTC", BTC_TOKEN_ADDRESS, TokenSource.SAMM);
        const wbtcLiquidity = new Big("2923");
        const wbtcValue = new TokenValue(wbtcToken, wbtcLiquidity, 6);

        const ETH_TOKEN_ADDRESS = new solana.PublicKey("2FPyTwcZLUg1MDrwsyoP4D6s1tM7hAkHYRjkNb5w6Pxk");
        const ethToken = new Token("ETH", ETH_TOKEN_ADDRESS, TokenSource.SAMM);
        const ethLiquidity = new Big("82339");
        const ethValue = new TokenValue(ethToken, ethLiquidity, 6);

        const calculator = new constantproduct.ConstantProductCalculator(wbtcValue, ethValue, fee);

        it("costForBuyingBaseValue() for 100 BTC", () => {
            const delta = new Big(100);
            const result = calculator.costForBuyingBaseValue(delta);

            expect(result.priceToken).to.equal(ethToken);
            expect(result.sizeToken).to.equal(wbtcToken);
            expect(result.size.toString()).to.equal("100");
            expect(result.price.toString()).to.equal("27.211175");
            expect(roughlyEqual(result.totalCost, new Big("2721.11750305997"))).to.be.true;
        });

        it("costForSellingBaseValue() for 100 BTC", () => {
            const delta = new Big(100);
            const result = calculator.costForSellingBaseValue(delta);

            expect(result.priceToken).to.equal(ethToken);
            expect(result.sizeToken).to.equal(wbtcToken);
            expect(result.size.toString()).to.equal("100");
            expect(result.price.toString()).to.equal("29.197399");
            expect(roughlyEqual(result.totalCost, new Big("2919.73994828014"))).to.be.true;
        });

        it("costForBuyingQuoteValue() for 100 ETH", () => {
            const delta = new Big(100);
            const result = calculator.costForBuyingQuoteValue(delta);

            expect(result.priceToken).to.equal(wbtcToken);
            expect(result.sizeToken).to.equal(ethToken);
            expect(result.size.toString()).to.equal("100");
            expect(result.price.toString()).to.equal("0.035421");
            expect(roughlyEqual(result.totalCost, new Big("3.54211058129067"))).to.be.true;
        });

        it("costForSellingQuoteValue() for 100 ETH", () => {
            const delta = new Big(100);
            const result = calculator.costForSellingQuoteValue(delta);

            expect(result.priceToken).to.equal(wbtcToken);
            expect(result.sizeToken).to.equal(ethToken);
            expect(result.size.toString()).to.equal("100");
            expect(result.price.toString()).to.equal("0.035578");
            expect(roughlyEqual(result.totalCost, new Big("3.55783333677891"))).to.be.true;
        });

        it("costForBuyingBaseValue() for 50 BTC", () => {
            const delta = new Big(50);
            const result = calculator.costForBuyingBaseValue(delta);

            expect(result.priceToken).to.equal(ethToken);
            expect(result.sizeToken).to.equal(wbtcToken);
            expect(result.size.toString()).to.equal("50");
            expect(result.price.toString()).to.equal("27.668363");
            expect(roughlyEqual(result.totalCost, new Big("1383.41817050405"))).to.be.true;
        });

        it("costForSellingBaseValue() for 50 BTC", () => {
            const delta = new Big(50);
            const result = calculator.costForSellingBaseValue(delta);

            expect(result.priceToken).to.equal(ethToken);
            expect(result.sizeToken).to.equal(wbtcToken);
            expect(result.size.toString()).to.equal("50");
            expect(result.price.toString()).to.equal("28.688748");
            expect(roughlyEqual(result.totalCost, new Big("1434.43740754276"))).to.be.true;
        });

        it("costForBuyingQuoteValue() for 50 ETH", () => {
            const delta = new Big(50);
            const result = calculator.costForBuyingQuoteValue(delta);

            expect(result.priceToken).to.equal(wbtcToken);
            expect(result.sizeToken).to.equal(ethToken);
            expect(result.size.toString()).to.equal("50");
            expect(result.price.toString()).to.equal("0.035443");
            expect(roughlyEqual(result.totalCost, new Big("1.77212902943938"))).to.be.true;
        });

        it("costForSellingQuoteValue() for 50 ETH", () => {
            const delta = new Big(50);
            const result = calculator.costForSellingQuoteValue(delta);

            expect(result.priceToken).to.equal(wbtcToken);
            expect(result.sizeToken).to.equal(ethToken);
            expect(result.size.toString()).to.equal("50");
            expect(result.price.toString()).to.equal("0.035557");
            expect(roughlyEqual(result.totalCost, new Big("1.77783469104907"))).to.be.true;
        });

        it("costForBuyingBaseValue() for 1 BTC", () => {
            const delta = new Big(1);
            const result = calculator.costForBuyingBaseValue(delta);

            expect(result.priceToken).to.equal(ethToken);
            expect(result.sizeToken).to.equal(wbtcToken);
            expect(result.size.toString()).to.equal("1");
            expect(result.price.toString()).to.equal("28.131563");
            expect(roughlyEqual(result.totalCost, new Big("28.1315626305004"))).to.be.true;
        });

        it("costForSellingBaseValue() for 1 BTC", () => {
            const delta = new Big(1);
            const result = calculator.costForSellingBaseValue(delta);

            expect(result.priceToken).to.equal(ethToken);
            expect(result.sizeToken).to.equal(wbtcToken);
            expect(result.size.toString()).to.equal("1");
            expect(result.price.toString()).to.equal("28.207176");
            expect(roughlyEqual(result.totalCost, new Big("28.2071756355872"))).to.be.true;
        });

        it("costForBuyingQuoteValue() for 1 ETH", () => {
            const delta = new Big(1);
            const result = calculator.costForBuyingQuoteValue(delta);

            expect(result.priceToken).to.equal(wbtcToken);
            expect(result.sizeToken).to.equal(ethToken);
            expect(result.size.toString()).to.equal("1");
            expect(result.price.toString()).to.equal("0.035464");
            expect(roughlyEqual(result.totalCost, new Big("0.035463651147438"))).to.be.true;
        });

        it("costForSellingQuoteValue() for 1 ETH", () => {
            const delta = new Big(1);
            const result = calculator.costForSellingQuoteValue(delta);

            expect(result.priceToken).to.equal(wbtcToken);
            expect(result.sizeToken).to.equal(ethToken);
            expect(result.size.toString()).to.equal("1");
            expect(result.price.toString()).to.equal("0.035536");
            expect(roughlyEqual(result.totalCost, new Big("0.035535512588467"))).to.be.true;
        });

        it("costForBuyingBaseProportion() for 1%", () => {
            // 1%
            const delta = new Big(0.01);
            const result = calculator.costForBuyingBaseProportion(delta);

            expect(result.priceToken).to.equal(ethToken);
            expect(result.sizeToken).to.equal(wbtcToken);
            expect(result.size.toString()).to.equal("29.23");
            expect(result.price.toString()).to.equal("27.862828");
            expect(roughlyEqual(result.totalCost, new Big("814.43044980643"))).to.be.true;
        });

        it("costForSellingBaseProportion() for 1%", () => {
            // 1%
            const delta = new Big(0.01);
            const result = calculator.costForSellingBaseProportion(delta);

            expect(result.priceToken).to.equal(ethToken);
            expect(result.sizeToken).to.equal(wbtcToken);
            expect(result.size.toString()).to.equal("29.23");
            expect(result.price.toString()).to.equal("28.482627");
            expect(roughlyEqual(result.totalCost, new Big("832.547187345321"))).to.be.true;
        });

        it("costForBuyingQuoteProportion() for 1%", () => {
            // 1%
            const delta = new Big(0.01);
            const result = calculator.costForBuyingQuoteProportion(delta);

            expect(result.priceToken).to.equal(wbtcToken);
            expect(result.sizeToken).to.equal(ethToken);
            expect(result.size.toString()).to.equal("823.39");
            expect(result.price.toString()).to.equal("0.035113");
            expect(roughlyEqual(result.totalCost, new Big("28.9119397221752"))).to.be.true;
        });

        it("costForSellingQuoteProportion() for 1%", () => {
            // 1%
            const delta = new Big(0.01);
            const result = calculator.costForSellingQuoteProportion(delta);

            expect(result.priceToken).to.equal(wbtcToken);
            expect(result.sizeToken).to.equal(ethToken);
            expect(result.size.toString()).to.equal("823.39");
            expect(result.price.toString()).to.equal("0.035894");
            expect(roughlyEqual(result.totalCost, new Big("29.5550763139022"))).to.be.true;
        });
    });

    describe("calculateUpdatedBalances() on USDC/ETH, fee 0.3%", () => {
        // Figures taken from USDC/ETH Uniswap pool on 11ᵗʰ September 2020.
        const baseLiquidity = new Big("41639824");
        const quoteLiquidity = new Big("114485");
        const fee = new Big("0.003");

        it("Correct result for BUYing 100 USDC", () => {
            const delta = new Big(100);
            const result = constantproduct.calculateUpdatedBalances(baseLiquidity, quoteLiquidity, delta, fee);
            expect(roughlyEqual(result.x, new Big("41639924"))).to.be.true;
            expect(roughlyEqual(result.y, new Big("114484.725884356"))).to.be.true;
        });

        it("Correct result for BUYing 100 ETH", () => {
            const delta = new Big(100);

            // For a SELL we have to switch the base and quote.
            const result = constantproduct.calculateUpdatedBalances(quoteLiquidity, baseLiquidity, delta, fee);
            expect(roughlyEqual(result.x, new Big("114585"))).to.be.true;
            expect(roughlyEqual(result.y, new Big("41603593.2427279"))).to.be.true;
        });

        it("Correct result for BUYing 1 USDC", () => {
            const delta = new Big(1);
            const result = constantproduct.calculateUpdatedBalances(baseLiquidity, quoteLiquidity, delta, fee);
            expect(roughlyEqual(result.x, new Big("41639825"))).to.be.true;
            expect(roughlyEqual(result.y, new Big("114484.997258837"))).to.be.true;
        });

        it("Correct result for BUYing 1 ETH", () => {
            const delta = new Big(1);

            // For a SELL we have to switch the base and quote.
            const result = constantproduct.calculateUpdatedBalances(quoteLiquidity, baseLiquidity, delta, fee);
            expect(roughlyEqual(result.x, new Big("114486"))).to.be.true;
            expect(roughlyEqual(result.y, new Big("41639461.3800673"))).to.be.true;
        });
    });

    describe("calculateUpdatedBalances() on WBTC/ETH, fee 0.1%", () => {
        // Figures taken from USDC/ETH Uniswap pool on 11ᵗʰ September 2020.
        const baseLiquidity = new Big("2923");
        const quoteLiquidity = new Big("82339");
        const fee = new Big("0.001");

        it("Correct result for BUYing 100 WBTC", () => {
            const delta = new Big(100);
            const result = constantproduct.calculateUpdatedBalances(baseLiquidity, quoteLiquidity, delta, fee);
            expect(roughlyEqual(result.x, new Big("3023"))).to.be.true;
            expect(roughlyEqual(result.y, new Big("79617.88249694"))).to.be.true;
        });

        it("Correct result for BUYing 100 ETH", () => {
            const delta = new Big(100);

            // For a SELL we have to switch the base and quote.
            const result = constantproduct.calculateUpdatedBalances(quoteLiquidity, baseLiquidity, delta, fee);
            expect(roughlyEqual(result.x, new Big("82439"))).to.be.true;
            expect(roughlyEqual(result.y, new Big("2919.45788941871"))).to.be.true;
        });

        it("Correct result for BUYing 50 WBTC", () => {
            const delta = new Big(50);
            const result = constantproduct.calculateUpdatedBalances(baseLiquidity, quoteLiquidity, delta, fee);
            expect(roughlyEqual(result.x, new Big("2973"))).to.be.true;
            expect(roughlyEqual(result.y, new Big("80955.581829496"))).to.be.true;
        });

        it("Correct result for BUYing 50 ETH", () => {
            const delta = new Big(50);

            // For a SELL we have to switch the base and quote.
            const result = constantproduct.calculateUpdatedBalances(quoteLiquidity, baseLiquidity, delta, fee);
            expect(roughlyEqual(result.x, new Big("82389"))).to.be.true;
            expect(roughlyEqual(result.y, new Big("2921.22787097056"))).to.be.true;
        });

        it("Correct result for BUYing 1 WBTC", () => {
            const delta = new Big(1);
            const result = constantproduct.calculateUpdatedBalances(baseLiquidity, quoteLiquidity, delta, fee);
            expect(roughlyEqual(result.x, new Big("2924"))).to.be.true;
            expect(roughlyEqual(result.y, new Big("82310.8684373695"))).to.be.true;
        });

        it("Correct result for BUYing 1 ETH", () => {
            const delta = new Big(1);

            // For a SELL we have to switch the base and quote.
            const result = constantproduct.calculateUpdatedBalances(quoteLiquidity, baseLiquidity, delta, fee);
            expect(roughlyEqual(result.x, new Big("82340"))).to.be.true;
            expect(roughlyEqual(result.y, new Big("2922.96453634885"))).to.be.true;
        });
    });
});