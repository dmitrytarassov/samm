"use strict";

const {
    expect
} = require("chai");
const log = require("../lib/log.js");
const {
    EnumValue,
    Enum
} = require("../lib/enums.js");

describe("Enums", () => {
    log.setLevels({
        trace: false,
        print: false
    });

    describe("EnumValue", () => {
        it("Constructor should successfully construct with valid parameters", () => {
            const actual = new EnumValue("test-value");
            expect(actual.value).to.equal("TEST-VALUE");
            expect(actual.isEnumValue).to.be.true;
            expect(actual.constructor.name).to.equal("EnumValue");
            expect(actual.toString()).to.equal("« TEST-VALUE »");
        });

        it("Constructor should throw with invalid name parameter", () => {
            const shouldThrow = () => new EnumValue({});
            expect(shouldThrow).to.throw();
        });
    });

    describe("Enum", () => {
        it("Constructor should successfully construct with valid parameters", () => {
            const allValues = [new EnumValue("test-value1"), new EnumValue("test-value2")];
            const actual = new Enum("TestEnum", allValues);
            expect(actual.isEnum).to.be.true;
            expect(actual.allValues).to.equal(allValues);
            expect(actual.typeName).to.equal("TestEnum");
            expect(actual.constructor.name).to.equal("Enum");
            expect(actual.toString()).to.equal("« 'TestEnum' enum with values [TEST-VALUE1, TEST-VALUE2] »");
        });

        it("Constructor should throw with invalid type name parameter", () => {
            const allValues = [new EnumValue("test-value1"), new EnumValue("test-value2")];
            const shouldThrow = () => new Enum({}, allValues);
            expect(shouldThrow).to.throw();
        });

        it("Constructor should throw with invalid all values parameter", () => {
            const shouldThrow = () => new Enum("TestEnum", [{}]);
            expect(shouldThrow).to.throw();
        });

        it("fromName() should return existing enum", () => {
            const allValues = [new EnumValue("test-value1"), new EnumValue("test-value2")];
            const actual = new Enum("TestEnum", allValues);
            expect(actual.fromName("test-value1")).to.equal(allValues[0]);
            expect(actual.fromName("test-value2")).to.equal(allValues[1]);
        });

        it("fromName() should return undefined enum if not found", () => {
            const noSuchEnum = new EnumValue("no-such-enum");
            const allValues = [new EnumValue("test-value1"), new EnumValue("test-value2")];
            const actual = new Enum("TestEnum", allValues, noSuchEnum);
            expect(actual.fromName("this-should-not-exist")).to.equal(noSuchEnum);
        });

        it("fromName() should throw if not found and no undefined specified", () => {
            const allValues = [new EnumValue("test-value1"), new EnumValue("test-value2")];
            const actual = new Enum("TestEnum", allValues);
            const shouldThrow = () => actual.fromName("this-should-not-exist");
            expect(shouldThrow).to.throw();
        });
    });
});