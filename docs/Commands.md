# 💉 Serum Automated Market Maker

## 📜 Commands

The following commands are available:

* [balance](#-command-balance)
* [cancel-orders](#-command-cancel-orders)
* [create-wallet](#-command-create-wallet)
* [events](#-command-events)
* [fills](#-command-fills)
* [init-market](#-command-init-market)
* [make-market](#-command-make-market)
* [openorders](#-command-openorders)
* [orderbook](#-command-orderbook)
* [orders](#-command-orders)
* [pricing](#-command-pricing)
* [rebalance](#-command-rebalance)
* [requests](#-command-requests)
* [settle](#-command-settle)
* [spread](#-command-spread)

## 📖 Command Reference

Note that if you’ve followed the [quickstart](Quickstart.md) you’ll need to prefix these commands with `/var/sammd/bin/samm` to run the container and pass the parameters. One exception is for `make-market`, where you may want to use `sammd` so the command runs as a daemon. You could just add `/var/sammd/bin` to your `$PATH` if that’s your thing. And of course none of this applies if you’ve downloaded the source and are running `node` directly.

### ⚡ Command: balance

The balance command reports on the SOL balance of the wallet account (both settled and unsettled), and optionally the balance of the SPL tokens in market accounts.

It also tries to give a ‘notional’ value for the market account by calculating how much the current ‘base’ balance is in ‘quote’ terms. For example, in the ETH/USDT market it calculates a total of how much USDT there is plus how much the ETH is worth in USDT terms (calculated using the best bid price).

#### Parameters

* `--market <market-name>` Specifies the market to be used. (Optional)

#### Example Use
```
$ /var/sammd/bin/samm balance --market ETH/USDT
2020-10-01T17:12:40.453Z 📄 Solana:                    1.97224368 SOL
2020-10-01T17:12:43.953Z 📄 Wrapped Ethereum:             4.38185 ETH (of which 0.07 ETH is unsettled)
2020-10-01T17:12:43.955Z 📄 Wrapped USDT:            1561.132013 USDT (of which 2.449217 USDT is unsettled)
2020-10-01T17:12:45.336Z 📄 Notional total:          3090.923485 USDT
```

### ⚡ Command: cancel-orders

Cancels all open orders for the specified market. Runs synchronously and waits for 6 confirmations of the cancellation before continuing to the next or exiting.

#### Parameters

* `--asynchronous` If specified, sends the cancel transaction but does not wait for confirmations. If unspecified, waits for the transaction to receive `--confirmations` confirmations. (Optional. Default: unspecified)
* `--confirmations <confirmations>` Specifies the number of confirmations to wait if running synchronously (i.e. `--asynchronous` is unspecified). (Optional. Default: 6)
* `--market <market-name>` Specifies the market to be used. (Required)
* `--simulate` If specified, run but don’t place any orders. (Optional. Default: unspecified)

#### Example Use

```
$ /var/sammd/bin/samm cancel-orders --market ETH/USDT
2020-10-01T17:16:21.325Z 📄 Cancelling order: BUY order                    1     349.12   0.00700
2020-10-01T17:16:34.564Z 📄 Cancellation confirmed: AHfw7JqEzYSs3weWaZsxFNyyL4Qv5SMcuFf1p9A2pgCG4fViSVdcaX4i9C9DukFE4fecQf23k33UUcAXnrQuJmw
2020-10-01T17:16:34.569Z 📄 Cancelling order: SELL order                   2     352.20   0.00700
2020-10-01T17:16:44.127Z 📄 Cancellation confirmed: veCLojtdLMgq8s6WS1g7kGTj22PD4kSEbuk1SKjLa6qM6BSoC7tkHtrrtwxubMgvhy2DogsyFMdwBYtZ138u6sh
```

### ⚡ Command: create-wallet

Creates a wallet and stores the private key mnemonic seed in the `wallet.json` file.

#### Parameters

* `--filename <filename>` Specifies the filename used when writing the mnemonic seed. (Optional. Default: `wallet.json`.)
* `--overwrite` If specified, the command will (try to) overwrite an existing wallet file. If unspecified, the command will error if there is an existing wallet file. (Optional. Default: unspecified)

#### Example Use

```
$ /var/sammd/bin/samm create-wallet --filename wallet2.json
2020-09-29T16:40:00.558Z 📄 Wallet file created at: wallet2.json
```

### ⚡ Command: events

Downloads the latest events for your account in the market.

#### Parameters

* `--json` If specified, events are output as JSON objects (for piping to other programs). If unspecified, events are output as formatted text. (Optional. Default: upspecified)
* `--market <market-name>` Specifies the market to be used. (Required)
* `--onlyMine` Filters out events that do not belong to one of the current account’s OpenOrders accounts.
* `--poll` Turns on polling mode, where the event stream is polled every `pollInterval` seconds. (Optional. Default: unspecified)
* `--pollInterval <seconds>` Specifies the number of seconds to wait between polling the stream for updates. Only used if `--poll` is specified. (Optional. Default: 30)

#### Example Use

```
$ /var/sammd/bin/samm events --market ETH/USDT
2020-10-01T15:53:39.570Z 📄 Events in market ETH/USDT:
2020-10-01T15:53:39.573Z 📄 {
  eventFlags: { fill: false, out: true, bid: true, maker: false },
  openOrdersSlot: 5,
  feeTier: 0,
  nativeQuantityReleased: <BN: 5f2f83840>,
  nativeQuantityPaid: <BN: 0>,
  nativeFeeOrRebate: <BN: 0>,
  orderId: <BN: 8d60ffffffffffeea451>,
  openOrders: PublicKey {
    _bn: <BN: f44c72e7ef3360a73e8374ce7ebfa56ee10ba6b34aa7222ce782efa971841e5>
  },
  clientOrderId: <BN: b137a94a98b4671b>
}
```

### ⚡ Command: fills

Downloads the latest fills for your account in the market.

#### Parameters

* `--json` If specified, events are output as JSON objects (for piping to other programs). If unspecified, events are output as formatted text. (Optional. Default: upspecified)
* `--limit <limit>` Specifies the maximum fills to return. (Optional. Default: Serum default of 100)
* `--market <market-name>` Specifies the market to be used. (Required)
* `--onlyMine` Filters out fills that do not belong to one of the current account’s OpenOrders accounts.
* `--poll` Turns on polling mode, where the event stream is polled every `pollInterval` seconds. (Optional. Default: unspecified)
* `--pollInterval <seconds>` Specifies the number of seconds to wait between polling the stream for updates. Only used if `--poll` is specified. (Optional. Default: 30)

#### Example Use

```
$ /var/sammd/bin/samm fills --market ETH/USDT
2020-10-01T15:52:36.217Z 📄 Fills in market ETH/USDT:
2020-10-01T15:52:36.221Z 📄
    eventFlags: {"fill":true,"out":false,"bid":false,"maker":false}
    openOrdersSlot: 0
    feeTier: 6
    nativeQuantityReleased: 2543354
    nativeQuantityPaid: 7000
    nativeFeeOrRebate: 2546
    orderId: 670889635216742683859653
    openOrders: PublicKey 22bwhg5FFXEQBCPF2my75tZGvqGc5hc461s4G8NZWSBv
    clientOrderId: 17533887066021367622
    side: sell
    price: 363.7
    feeCost: 0.002546
    size: 0.007
2020-10-01T15:52:36.226Z 📄
    eventFlags: {"fill":true,"out":false,"bid":true,"maker":true}
    openOrdersSlot: 0
    feeTier: 0
    nativeQuantityReleased: 7000
    nativeQuantityPaid: 2545137
    nativeFeeOrRebate: 763
    orderId: 670926528704890100688399
    openOrders: PublicKey F9epqyzZ1ZWfmViNZM4Mj2Y4qhZ47qj3T5B9pUZBGFv2
    clientOrderId: 11
    side: buy
    price: 363.7
    feeCost: -0.000763
    size: 0.007
```

### ⚡ Command: init-market

Creates any required SPL token accounts in your wallet for the base and quote tokens. If either exists already, it is left unaltered. If both already exist, nothing is done.

#### Parameters

* `--market <market-name>` Specifies the market to be used. (Required)
* `--simulate` If specified, run but don’t place any orders. (Optional. Default: unspecified)

#### Example Use

```
$ /var/sammd/bin/samm init-market --market SRM/USDC --simulate
2020-09-29T19:00:00.538Z 📄 No account for base token SRM. Creating...
2020-09-29T19:00:00.540Z 📄 Account created for base token SRM at address «no-account-created-simulate
2020-09-29T19:00:00.950Z 📄 No account for quote token USDC. Creating...
2020-09-29T19:00:00.950Z 📄 Account created for quote token USDC at address «no-account-created-simulate
```

### ⚡ Command: make-market

Starts the market-making server, which:
* checks its open orders are as expected, and if not
* cancels any open orders
* calculates the implied prices of the tokens
* places buy and sell orders
* waits for a while
* repeats

#### Parameters

* `--auditLogDirectory <directory-path>` Path to a writable directory where the server can create and write CSV audit logs for filled orders. (Optional. Default: ./auditlogs)
* `--simulate` If specified, run but don’t place any orders (will involve recalculating orders every poll). (Optional. Default: unspecified)
* `--feePercentage <percentage>` Percentage to widen spread to gather fees (Optional.Default: 0.3, meaning 0.3%).
* `--market <market-name>` Specifies the market to be used. (Required)
* `--pollInterval <seconds>` How long to wait before checking orders. (Optional. Default: 30 seconds)
* `--positionSizePercentage <percentage>` What percentact of your balances to allocate to the orders. (A higher proportion will widen the spread derived by the constant-product function, meaning it is less likely the order will be filled.) (Optional. Default: 0.1, meaning 0.1%)

#### Example Use

```
$ /var/sammd/bin/sammd make-market --market ETH/USDT --simulate
af1234008869c826a4a6c3f98c34de9effb8a5ac76e004658ae5012b54e3e779
2020-10-01T13:02:02.987Z 📄 Fee is 0.30%
2020-10-01T13:02:04.741Z 📄 Base balance: « 4.31885 ETH »
2020-10-01T13:02:04.741Z 📄 Quote balance: « 1583.782555 USDT »
2020-10-01T13:02:04.741Z 📄 Position size is 0.1000%
2020-10-01T13:02:04.743Z 📄 Minimum order size of 0.001 implies minimum base liquidity of 1 Wrapped Ethereum (ETH)
2020-10-01T13:02:04.743Z 📄 Tick size is 0.01 Wrapped USDT
2020-10-01T13:02:05.243Z 📄 Current spread is « 367.25 / 367.29 USDT »
2020-10-01T13:02:05.246Z 📄 Current implied price is 365.50 Wrapped USDT for 1 Wrapped Ethereum
2020-10-01T13:02:06.236Z 📄 Balances: « « 4.31885 ETH », « 1583.782555 USDT » »
2020-10-01T13:02:06.887Z 📄 Spread: « 367.25 / 367.26 USDT »
2020-10-01T13:02:06.887Z 📄 Buy price is 365.2 - lower than best ask in spread « 367.25 / 367.26 USDT »
2020-10-01T13:02:06.887Z 📄 Sell price is 368.2 - higher than best bid in spread « 367.25 / 367.26 USDT »
2020-10-01T13:02:06.888Z 📄 Orders to place:
  « Order »
    side:      buy
    orderType: postOnly
    clientId:  1
    owner:     CsKuESr3J5uNXaGwUhXucP3uoSPGU8pTWwb1Sa5mf2MC
    payer:     EuFT3Dxhuxsav2j8BFRnkqt9HmyPvr6tvchMR53WgoYQ
    price:     365.2
    size:      0.004319

  « Order »
    side:      sell
    orderType: postOnly
    clientId:  2
    owner:     CsKuESr3J5uNXaGwUhXucP3uoSPGU8pTWwb1Sa5mf2MC
    payer:     5S3YrMvnFSJoDXyEbXzrka9wrppKjBAZvAfSVTErdWiM
    price:     368.2
    size:      0.004319

2020-10-01T13:02:06.892Z 📄 Simulation - not placing order: BUY order                    1     365.20   0.00432
2020-10-01T13:02:06.892Z 📄 Simulation - not placing order: SELL order                   2     368.20   0.00432
2020-10-01T13:02:06.892Z 📄 Sleeping for 30 seconds...
```

### ⚡ Command: openorders

Outputs information from your ‘openorders’ Serum account for this market.

#### Parameters

* `--market <market-name>` Specifies the market to be used. (Required)

#### Example Use

```
$ /var/sammd/bin/samm openorders --market ETH/USDT
2020-10-01T13:39:19.382Z 📄 Address: F9epqyzZ1ZWfmViNZM4Mj2Y4qhZ47qj3T5B9pUZBGFv2
2020-10-01T13:39:19.389Z 📄 Market: 5abZGhrELnUnfM9ZUnvK6XJPoBU5eShZwfFPkdhAC7o
2020-10-01T13:39:19.394Z 📄 Owner: CsKuESr3J5uNXaGwUhXucP3uoSPGU8pTWwb1Sa5mf2MC
2020-10-01T13:39:19.395Z 📄 Base Token (Total): 0
2020-10-01T13:39:19.395Z 📄 Base Token (Free): 0
2020-10-01T13:39:19.396Z 📄 Quote Token (Total): 0
2020-10-01T13:39:19.397Z 📄 Quote Token (Free): 0
2020-10-01T13:39:19.397Z 📄 No Order IDs
2020-10-01T13:39:19.398Z 📄 No Client IDs
```

### ⚡ Command: orderbook

Prints out a summary of the top 5 bids and asks in the current order book for the market.

#### Parameters

* `--market <market-name>` Specifies the market to be used. (Required)

#### Example Use

```
$ /var/sammd/bin/samm orderbook --market ETH/USDT
2020-10-01T17:13:38.339Z 📄 ETH/USDT orderbook:
2020-10-01T17:13:38.349Z 📄 SELL order 10714193636572907206     352.42 206.68800
2020-10-01T17:13:38.351Z 📄 SELL order                    2     352.20   0.00700
2020-10-01T17:13:38.351Z 📄 SELL order 14934375987808497412     351.47 156.19800
2020-10-01T17:13:38.352Z 📄 SELL order 12153980845202835738     350.99 101.30800
2020-10-01T17:13:38.352Z 📄 SELL order  1601572306039540654     350.08  10.00000
2020-10-01T17:13:38.353Z 📄 BUY order                     1     349.12   0.00700
2020-10-01T17:13:38.354Z 📄 BUY order  17416873742967752133     348.79 115.75200
2020-10-01T17:13:38.354Z 📄 BUY order   1601572306009938288     348.61  50.00000
2020-10-01T17:13:38.355Z 📄 BUY order  10677784067903120005     348.36 183.43700
2020-10-01T17:13:38.355Z 📄 BUY order  11990816109917137919     348.09 380.52400
```

### ⚡ Command: orders

Prints out a summary of any open orders you have in the specified market.

#### Parameters

* `--market <market-name>` Specifies the market to be used. (Required)

#### Example Use

```
$ /var/sammd/bin/samm orders --market ETH/USDT
2020-10-01T17:14:28.581Z 📄 Open orders in market ETH/USDT for owner CsKuESr3J5uNXaGwUhXucP3uoSPGU8pTWwb1Sa5mf2MC:
2020-10-01T17:14:28.586Z 📄 BUY                     1     349.12   0.00700
2020-10-01T17:14:28.587Z 📄 SELL                    2     352.20   0.00700
```

### ⚡ Command: pricing

Prints out pricing information for the specified market, based on your current balances and the current orderbook. (You can see the implied pricing from balances is quite different from the current spread pricing - this generally shouldn’t be the case and is a warning sign, and maybe an opportunity to use the `rebalance` command.)

#### Parameters

* `--market <market-name>` Specifies the market to be used. (Required)
* `--feePercentage <percentage>` Percentage to widen spread to gather fees (Optional.Default: 0.3, meaning 0.3%).
* `--positionSizePercentage <percentage>` What proportion of your balances to allocate to the orders. (A higher proportion will widen the spread derived by the constant-product function, meaning it is less likely the order will be filled.) (Optional. Default: 0.1, meaning 0.1%)

#### Example Use

```
$ /var/sammd/bin/samm pricing --market ETH/USDT
2020-10-01T13:34:03.770Z 📄 Fee is 0.30%
2020-10-01T13:34:05.435Z 📄 Base balance: « 4.31885 ETH »
2020-10-01T13:34:05.437Z 📄 Quote balance: « 1583.782555 USDT »
2020-10-01T13:34:05.437Z 📄 Position size proportion is 0.1000%
2020-10-01T13:34:05.439Z 📄 Minimum order size of 0.001 implies minimum base liquidity of 1 Wrapped Ethereum (ETH)
2020-10-01T13:34:05.440Z 📄 Tick size is 0.01 Wrapped USDT
2020-10-01T13:34:06.102Z 📄 Current spread is « 367.75 / 367.79 USDT »
2020-10-01T13:34:06.114Z 📄 Current implied price is 365.2 / 368.2 Wrapped USDT for 1 Wrapped Ethereum
2020-10-01T13:34:06.114Z 📄 Current implied lot size is 0.004319 / 0.004319 Wrapped Ethereum
```
You can use the `pricing` command to see the effect of different choices of fees and position sizes:
```
/var/sammd/bin/samm pricing --market ETH/USDT --feePercentage 0.1 --positionSizePercentage 0.05
2020-10-01T13:35:07.379Z 📄 Fee is 0.10%
2020-10-01T13:35:09.507Z 📄 Base balance: « 4.31885 ETH »
2020-10-01T13:35:09.509Z 📄 Quote balance: « 1583.782555 USDT »
2020-10-01T13:35:09.509Z 📄 Position size proportion is 0.0500%
2020-10-01T13:35:09.511Z 📄 Minimum order size of 0.001 implies minimum base liquidity of 2 Wrapped Ethereum (ETH)
2020-10-01T13:35:09.512Z 📄 Tick size is 0.01 Wrapped USDT
2020-10-01T13:35:09.840Z 📄 Current spread is « 367.55 / 367.68 USDT »
2020-10-01T13:35:09.846Z 📄 Current implied price is 366.2 / 367.3 Wrapped USDT for 1 Wrapped Ethereum
2020-10-01T13:35:09.847Z 📄 Current implied lot size is 0.002159 / 0.002159 Wrapped Ethereum
```

### ⚡ Command: rebalance

If you’re just setting things up or if token balances have shifted, the `rebalance` command will try to place an order to buy or sell tokens to move the ratio of balances back towards the current live spread.

It won’t be exact but it aims to get balances back to where the constant-product function will be self-correcting.

It takes a `--simulate` parameter to enable you to run this safely without it actually placing the order. It’s often best to try a simulated run before running this command for real.

#### Parameters

* `--market <market-name>` Specifies the market to be used. (Required)
* `--simulate` If specified, run but don’t place any orders. (Optional. Default: unspecified)
* `--useMidPrice` If specified, uses the mid price instead of the best bid price (for a buy) or the best ask price (for a sell). (Optional. Default: unspecified)

#### Example Use

```
$ /var/sammd/bin/samm rebalance --market ETH/USDT --simulate
2020-10-01T12:32:27.459Z 📄 Spread: « 367.31 / 367.32 USDT »
2020-10-01T12:32:27.463Z 📄 Mid price: 367.315 USDT
2020-10-01T12:32:27.465Z 📄 Ratio should be: 0.00272245892490097056
2020-10-01T12:32:29.461Z 📄 Base balance: « 8.63585 ETH »
2020-10-01T12:32:29.462Z 📄 Quote balance: « 0 USDT »
2020-10-01T12:32:29.463Z 📄 Ratio is: infinity
2020-10-01T12:32:29.464Z 📄 Base in quote terms is: 3172.07724275
2020-10-01T12:32:29.465Z 📄 Total in quote terms is: « 3172.07724275 USDT »
2020-10-01T12:32:29.466Z 📄 Desired base value: « 4.31792500000000000538186172 ETH »
2020-10-01T12:32:29.466Z 📄 Desired quote value: « 1586.038621375 USDT »
2020-10-01T12:32:29.467Z 📄 Change in base: « -4.31792499999999999461813828 ETH »
2020-10-01T12:32:29.468Z 📄 Sell « -4.31792499999999999461813828 ETH »
2020-10-01T12:32:29.469Z 📄 Order: {
  owner: Account {
    _keypair: { publicKey: [Uint8Array], secretKey: [Uint8Array] }
  },
  payer: PublicKey {
    _bn: <BN: 41d926783cb907c6df1f77a8a3905e7d3fc74a28772746d4d7379bdb528616c2>
  },
  side: 'sell',
  price: 367.32,
  size: 4.317925,
  orderType: 'limit'
}
2020-10-01T12:32:29.478Z 📄 Simulation - not placing order: SELL              <no ID>     367.32   4.31792
```


### ⚡ Command: requests

Downloads the latest requests for your account in the market.

#### Parameters

* `--json` If specified, requests are output as JSON objects (for piping to other programs). If unspecified, requests are output as formatted text. (Optional. Default: upspecified)
* `--limit <limit>` Specifies the maximum fills to return. (Optional. Default: Serum default of 100)
* `--market <market-name>` Specifies the market to be used. (Required)
* `--onlyMine` Filters out requests that do not belong to one of the current account’s OpenOrders accounts.
* `--poll` Turns on polling mode, where the request stream is polled every `pollInterval` seconds. (Optional. Default: unspecified)
* `--pollInterval <seconds>` Specifies the number of seconds to wait between polling the stream for updates. Only used if `--poll` is specified. (Optional. Default: 30)

#### Example Use

```
$ /var/sammd/bin/samm requests --market ETH/USDT
2020-10-22T12:54:28.472Z 📄 Requests in market ETH/USDT:
2020-10-22T13:17:25.043Z 📄
  « Request »
    flags: cancelOrder
    orderId: 816415999214237338617045
    clientOrderId: 0
    openOrdersSlot: 13
    feeTier: 0
    nativeQuoteQuantityLocked: 0
    openOrders: 5S3YrMvnFSJoDXyEbXzrka9wrppKjBAZvAfSVTErdWiM
```

### ⚡ Command: settle

When an order is placed, tokens are taken from your SPL token account and locked away. When the order is filled, those tokens are removed and replaced by the tokens you just bought or sold. Those tokens are yours, but they’re not yet in your SPL token account. Moving them from this transactional area to your SPL token account is called ‘settling’, and that’s what the `settle` command does.

You can see this in action when the `balances` command tells you how many tokens you have, and how many of those are ‘unsettled’. If you see an ‘unsettled’ balance and you have no open orders locking those tokens, you can use this command to move the tokens to your SPL token accounts.

#### Parameters

* `--asynchronous` If specified, sends the settle transaction but does not wait for confirmations. If unspecified, waits for the transaction to receive `--confirmations` confirmations. (Optional. Default: unspecified)
* `--confirmations <confirmations>` Specifies the number of confirmations to wait if running synchronously (i.e. `--asynchronous` is unspecified). (Optional. Default: 6)
* `--simulate` If specified, run but don’t place any orders. (Optional. Default: unspecified)
* `--market <market-name>` Specifies the market to be used. (Required)

#### Example Use

```
$ /var/sammd/bin/samm settle --market ETH/USDT
2020-10-01T17:17:46.976Z 📄 Settling: 5abZGhrELnUnfM9ZUnvK6XJPoBU5eShZwfFPkdhAC7o 0.07ETH free (of 0.077ETH), 2.44384USDT free (of 2.44384USDT)
2020-10-01T17:17:47.487Z 📄 Waiting for: 3WkUMuKqCgeocap6jKPCmwxaMgs4wZQ14AxZU6m5fF6AzygXw5samRThd7wouNGEJBRdcdykZ8zGUSy3VoLGMmia2
2020-10-01T17:18:19.132Z 📄 Settled: 3WkUMuKqCgeocap6jKPCmwxaMgs4wZQ14AxZU6m5fF6AzygXw5samRThd7wouNGEJBRdcdykZ8zGUSy3VoLGMmia
```

### ⚡ Command: spread

Prints out the current spread from the live orderbook for the specified market.

#### Parameters

* `--market <market-name>` Specifies the market to be used. (Required)

#### Example Use

```
$ /var/sammd/bin/samm spread --market ETH/USDT
2020-10-01T17:15:12.824Z 📄 Spread: « 349.12 / 350.08 USDT »
```
