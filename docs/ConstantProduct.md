# 💉 Serum Automated Market Maker

## 🗺️ Overview

SAMM uses a ‘_constant product_’ function for pricing. This is the same approach used by [Uniswap](https://uniswap.org/) and other Automated Market Makers. [Runtime Verification](https://runtimeverification.com/) have a good paper on the subject: [Formal Specification of Constant Product (x × y = k) Market Maker Model and Implementation by Yi Zhang, Xiaohong Chen, and Daejun Park](https://github.com/runtimeverification/verified-smart-contracts/blob/uniswap/uniswap/x-y-k.pdf)

If you want to try some contant product calculations interactively, there’s a LibreOffice [spreadsheet](../assets/Constant%20Product%20Calculations.ods) available.


## ✖️ Constant Product Calculation

Let’s say for a pair of tokens, `base` and `quote`, you have `b` `base` tokens and `q` `quote` tokens. Then if you multiply `b` and `q` you get another number `k`:

> b ✖️ q = k

`k` is the product of `b` and `q`.

Now, here’s the interesting question. If you want to sell `x` amount of `base`, how much `quote` should you charge?

Well, the idea of the ‘constant product’ function is that, after the transaction - when you have `new-b` and `new-q` - these two balances should still multiply together to give the same `k`.

> new-b ✖️ new-q = k

Not a new `k`, the exact same `k`.

Since we’re selling `x` `base`, `new-b` is just `b - x`.

> (b - x) ✖️ new-q = k

Now, `new-q` is really just `q` plus the amount of `quote` we want to charge to maintain the constant product. So if we call this charge `y`:

> (b - x) ✖️ (q + y) = k

And `y` is what we’re interested in calculating - it’s how much we should charge for `x` `base`. It’s also the only unknown in the above equation so calculating it is straightforward.


## 💉 SAMM’s Adaptations Of Constant Product Calculations

That model works well for on-demand systems like Uniswap, where you specify how much `base` you want to buy or sell. Serum however implements orderbooks, so SAMM needs to place specific orders on the orderbook, offering to buy or sell at specific prices.

So one tweak is that SAMM calculates the ‘constant product’ price for a specific proportion of the amount of `base` it has. This is configurable, but by default it’s 1% of the `base` balance.

The 1% is a compromise. Ideally we want it to be **higher**, so we’re selling more of `base` at our specified price. And ideally we want it to be **lower**, because the constant product function skews the price-per-b-token higher the more `base` we add, and that leads to a wider spread, which leads to far fewer trades.

(What’s the best proportion to use? I don’t know. That’s why it’s configurable.)

Another tweak to the constant product is the fee. SAMM takes into account a fee (configurable, but 0.3% by default) when calculating the price and size of orders. Again this is a compromise. A **higher** fee means more profit but fewer trades. A **lower** fee means less profit but more trades.

(What’s the best fee to use? I don’t know that either. That’s why it too is configurable.)
